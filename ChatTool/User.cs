﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ChatTool
{
    /// <summary>
    /// 用户类，记录用户的信息
    /// </summary>
    [Serializable]
    public class User
    {
        private string name;//用户名， 长度<=6
        /// <summary>
        /// 用户名， 长度<=6
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string password;//用户密码 
        /// <summary>
        /// 用户密码 
        /// </summary>
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        private UserState loginState;//登陆状态 在线，隐身，忙碌，离线，离开
        /// <summary>
        /// 登陆状态 在线，隐身，忙碌，离线，离开
        /// </summary>
        public UserState LoginState
        {
            get { return loginState; }
            set { loginState = value; }
        }
        private string signWord;//个性签名
        /// <summary>
        /// 个性签名
        /// </summary>
        public string SignWord
        {
            get { return signWord; }
            set { signWord = value; }
        }
        private string sex;//性别
        /// <summary>
        /// 性别
        /// </summary>
        public string Sex
        {
            get { return sex; }
            set { sex = value; }
        }
        private string age;//年龄
        /// <summary>
        /// 年龄
        /// </summary>
        public string Age
        {
            get { return age; }
            set { age = value; }
        }
        private string address;//所在地
        /// <summary>
        /// 所在地
        /// </summary>
        public string Address
        {
            get { return address; }
            set { address = value; }
        }
        private Image photo;//个人照片形象
        /// <summary>
        /// 个人照片形象
        /// </summary>
        public Image Photo
        {
            get { return photo; }
            set { photo = value; }
        }
        private string ip;//ip地址
        /// <summary>
        /// 本机ip地址
        /// </summary>
        public string Ip
        {
            get { return ip; }
            set { ip = value; }
        }
        private int listenerPort;//监听端口
        /// <summary>
        /// 监听端口
        /// </summary>
        public int ListenerPort
        {
            get { return listenerPort; }
            set { listenerPort = value; }
        }

        private string loginDateTime;//登陆起始时间
        /// <summary>
        /// 登陆起始时间
        /// </summary>
        public string LoginDateTime
        {
            get { return loginDateTime; }
            set { loginDateTime = value; }
        }
        private string like;//喜好
        /// <summary>
        /// 喜好
        /// </summary>
        public string Like
        {
            get { return like; }
            set { like = value; }
        }
        public User() { }
        public User(User user)
        {
            this.name = user.name;
            this.password = user.password;
            this.loginState = user.loginState;
            this.address = user.address;
            this.age = user.age;
            this.ip = user.ip;
            this.like = user.like;
            this.listenerPort = user.listenerPort;
            this.loginDateTime = user.loginDateTime;
            if (user.photo != null)
                this.photo = new Bitmap(user.photo);
            this.sex = user.sex;
            this.signWord = user.signWord;
        }
        //public User Clone()
        //{
        //    return (User)this.MemberwiseClone();
        //}
    }
    /// <summary>
    /// 登陆状态 在线，隐身，忙碌，离线，离开
    /// </summary>
    [Serializable]
    public enum UserState
    { 
        /// <summary>
        /// 在线
        /// </summary>
        Online,
        /// <summary>
        /// 隐身
        /// </summary>
        Hide,
        /// <summary>
        ///  忙碌
        /// </summary>
        Busy,
        /// <summary>
        /// 离线
        /// </summary>
        OutLine,
        /// <summary>
        /// 离开
        /// </summary>
        Leave
    }
}
