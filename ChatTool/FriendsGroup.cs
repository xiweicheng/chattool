﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChatTool
{
    /// <summary>
    /// 好友组
    /// </summary>
    [Serializable]
    public class FriendsGroup
    {
        private string name;//组名
        /// <summary>
        /// 组名
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private List<string> ipList = new List<string>();//改组下包含的成员的IP
        /// <summary>
        /// 改组下包含的成员的IP
        /// </summary>
        public List<string> IpList
        {
            get { return ipList; }
            set { ipList = value; }
        }

        //private User[] users;//该组下的成员
        ///// <summary>
        ///// 该组下的成员
        ///// </summary>
        //public User[] Users
        //{
        //    get { return users; }
        //    set { users = value; }
        //}
        public FriendsGroup(string name)
        {
            this.name = name;
        }
    }
    [Serializable]
    public class Friend
    {
        private string realName;
        /// <summary>
        /// 真实网名
        /// </summary>
        public string RealName
        {
            get { return realName; }
            set { realName = value; }
        }
        private string virtualName;
        /// <summary>
        /// 自己命名
        /// </summary>
        public string VirtualName
        {
            get { return virtualName; }
            set { virtualName = value; }
        }
        private string ip;
        /// <summary>
        /// 所在IP
        /// </summary>
        public string Ip
        {
            get { return ip; }
            set { ip = value; }
        }
    }
}
