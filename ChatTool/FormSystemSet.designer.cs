﻿namespace ChatTool
{
    partial class FormSystemSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.buttonSaveOwnInfo = new System.Windows.Forms.Button();
            this.tbLike = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbSex = new System.Windows.Forms.ComboBox();
            this.tbAge = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pbHead = new System.Windows.Forms.PictureBox();
            this.tbSignword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonUse = new System.Windows.Forms.Button();
            this.cbTopmost = new System.Windows.Forms.CheckBox();
            this.cbChatToOneRecord = new System.Windows.Forms.CheckBox();
            this.cbAutoRun = new System.Windows.Forms.CheckBox();
            this.cbRockVioce = new System.Windows.Forms.CheckBox();
            this.cbOnlineWindow = new System.Windows.Forms.CheckBox();
            this.cbOnlineVoice = new System.Windows.Forms.CheckBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.cbGroupRecord = new System.Windows.Forms.CheckBox();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHead)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(322, 342);
            this.tabControl.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(314, 316);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "个人信息设置";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox
            // 
            this.groupBox.Controls.Add(this.buttonSaveOwnInfo);
            this.groupBox.Controls.Add(this.tbLike);
            this.groupBox.Controls.Add(this.label9);
            this.groupBox.Controls.Add(this.tbAddress);
            this.groupBox.Controls.Add(this.label8);
            this.groupBox.Controls.Add(this.label7);
            this.groupBox.Controls.Add(this.cbSex);
            this.groupBox.Controls.Add(this.tbAge);
            this.groupBox.Controls.Add(this.label6);
            this.groupBox.Controls.Add(this.groupBox2);
            this.groupBox.Controls.Add(this.tbSignword);
            this.groupBox.Controls.Add(this.label3);
            this.groupBox.Controls.Add(this.tbPassword);
            this.groupBox.Controls.Add(this.label2);
            this.groupBox.Controls.Add(this.tbName);
            this.groupBox.Controls.Add(this.label1);
            this.groupBox.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox.Location = new System.Drawing.Point(3, 3);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(304, 301);
            this.groupBox.TabIndex = 1;
            this.groupBox.TabStop = false;
            this.groupBox.Text = "个人信息设置";
            // 
            // buttonSaveOwnInfo
            // 
            this.buttonSaveOwnInfo.Location = new System.Drawing.Point(174, 251);
            this.buttonSaveOwnInfo.Name = "buttonSaveOwnInfo";
            this.buttonSaveOwnInfo.Size = new System.Drawing.Size(75, 32);
            this.buttonSaveOwnInfo.TabIndex = 15;
            this.buttonSaveOwnInfo.Text = "保存设置";
            this.buttonSaveOwnInfo.UseVisualStyleBackColor = true;
            this.buttonSaveOwnInfo.Click += new System.EventHandler(this.buttonSaveOwnInfo_Click);
            // 
            // tbLike
            // 
            this.tbLike.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbLike.Location = new System.Drawing.Point(67, 150);
            this.tbLike.Name = "tbLike";
            this.tbLike.Size = new System.Drawing.Size(213, 21);
            this.tbLike.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(21, 153);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 14);
            this.label9.TabIndex = 13;
            this.label9.Text = "爱好:";
            // 
            // tbAddress
            // 
            this.tbAddress.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbAddress.Location = new System.Drawing.Point(67, 122);
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(213, 21);
            this.tbAddress.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(21, 123);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 14);
            this.label8.TabIndex = 11;
            this.label8.Text = "住址:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(154, 224);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 14);
            this.label7.TabIndex = 10;
            this.label7.Text = "年龄:";
            // 
            // cbSex
            // 
            this.cbSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSex.FormattingEnabled = true;
            this.cbSex.Items.AddRange(new object[] {
            "男",
            "女",
            "保密"});
            this.cbSex.Location = new System.Drawing.Point(202, 186);
            this.cbSex.Name = "cbSex";
            this.cbSex.Size = new System.Drawing.Size(78, 22);
            this.cbSex.TabIndex = 9;
            // 
            // tbAge
            // 
            this.tbAge.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbAge.Location = new System.Drawing.Point(202, 220);
            this.tbAge.Name = "tbAge";
            this.tbAge.Size = new System.Drawing.Size(78, 21);
            this.tbAge.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(154, 190);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 14);
            this.label6.TabIndex = 7;
            this.label6.Text = "性别:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pbHead);
            this.groupBox2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox2.Location = new System.Drawing.Point(20, 190);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(88, 93);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "个人头像";
            // 
            // pbHead
            // 
            this.pbHead.BackColor = System.Drawing.Color.White;
            this.pbHead.Location = new System.Drawing.Point(6, 20);
            this.pbHead.Name = "pbHead";
            this.pbHead.Size = new System.Drawing.Size(70, 67);
            this.pbHead.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbHead.TabIndex = 3;
            this.pbHead.TabStop = false;
            this.pbHead.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pbHead_MouseDoubleClick);
            // 
            // tbSignword
            // 
            this.tbSignword.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbSignword.Location = new System.Drawing.Point(93, 90);
            this.tbSignword.Name = "tbSignword";
            this.tbSignword.Size = new System.Drawing.Size(187, 21);
            this.tbSignword.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(17, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 14);
            this.label3.TabIndex = 4;
            this.label3.Text = "个性签名:";
            // 
            // tbPassword
            // 
            this.tbPassword.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbPassword.Location = new System.Drawing.Point(93, 58);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(187, 21);
            this.tbPassword.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(17, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 14);
            this.label2.TabIndex = 2;
            this.label2.Text = "用户密码:";
            // 
            // tbName
            // 
            this.tbName.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbName.Location = new System.Drawing.Point(93, 25);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(187, 21);
            this.tbName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(17, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "用户姓名:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.cbGroupRecord);
            this.tabPage2.Controls.Add(this.buttonUse);
            this.tabPage2.Controls.Add(this.cbTopmost);
            this.tabPage2.Controls.Add(this.cbChatToOneRecord);
            this.tabPage2.Controls.Add(this.cbAutoRun);
            this.tabPage2.Controls.Add(this.cbRockVioce);
            this.tabPage2.Controls.Add(this.cbOnlineWindow);
            this.tabPage2.Controls.Add(this.cbOnlineVoice);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(314, 316);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "系统配置设置";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // buttonUse
            // 
            this.buttonUse.Location = new System.Drawing.Point(107, 224);
            this.buttonUse.Name = "buttonUse";
            this.buttonUse.Size = new System.Drawing.Size(75, 30);
            this.buttonUse.TabIndex = 7;
            this.buttonUse.Text = "应用";
            this.buttonUse.UseVisualStyleBackColor = true;
            this.buttonUse.Click += new System.EventHandler(this.buttonUse_Click);
            // 
            // cbTopmost
            // 
            this.cbTopmost.AutoSize = true;
            this.cbTopmost.Location = new System.Drawing.Point(95, 157);
            this.cbTopmost.Name = "cbTopmost";
            this.cbTopmost.Size = new System.Drawing.Size(96, 16);
            this.cbTopmost.TabIndex = 5;
            this.cbTopmost.Text = "显示在最前端";
            this.cbTopmost.UseVisualStyleBackColor = true;
            this.cbTopmost.CheckedChanged += new System.EventHandler(this.cbTopmost_CheckedChanged);
            // 
            // cbChatToOneRecord
            // 
            this.cbChatToOneRecord.AutoSize = true;
            this.cbChatToOneRecord.Checked = true;
            this.cbChatToOneRecord.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbChatToOneRecord.Location = new System.Drawing.Point(95, 113);
            this.cbChatToOneRecord.Name = "cbChatToOneRecord";
            this.cbChatToOneRecord.Size = new System.Drawing.Size(96, 16);
            this.cbChatToOneRecord.TabIndex = 4;
            this.cbChatToOneRecord.Text = "保存私聊记录";
            this.cbChatToOneRecord.UseVisualStyleBackColor = true;
            // 
            // cbAutoRun
            // 
            this.cbAutoRun.AutoSize = true;
            this.cbAutoRun.Location = new System.Drawing.Point(95, 179);
            this.cbAutoRun.Name = "cbAutoRun";
            this.cbAutoRun.Size = new System.Drawing.Size(96, 16);
            this.cbAutoRun.TabIndex = 3;
            this.cbAutoRun.Text = "开机自动启动";
            this.cbAutoRun.UseVisualStyleBackColor = true;
            this.cbAutoRun.CheckedChanged += new System.EventHandler(this.cbAutoRun_CheckedChanged);
            // 
            // cbRockVioce
            // 
            this.cbRockVioce.AutoSize = true;
            this.cbRockVioce.Checked = true;
            this.cbRockVioce.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbRockVioce.Location = new System.Drawing.Point(95, 91);
            this.cbRockVioce.Name = "cbRockVioce";
            this.cbRockVioce.Size = new System.Drawing.Size(132, 16);
            this.cbRockVioce.TabIndex = 2;
            this.cbRockVioce.Text = "窗口震动时接收声音";
            this.cbRockVioce.UseVisualStyleBackColor = true;
            // 
            // cbOnlineWindow
            // 
            this.cbOnlineWindow.AutoSize = true;
            this.cbOnlineWindow.Checked = true;
            this.cbOnlineWindow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbOnlineWindow.Location = new System.Drawing.Point(95, 69);
            this.cbOnlineWindow.Name = "cbOnlineWindow";
            this.cbOnlineWindow.Size = new System.Drawing.Size(132, 16);
            this.cbOnlineWindow.TabIndex = 1;
            this.cbOnlineWindow.Text = "好友上下线窗口提示";
            this.cbOnlineWindow.UseVisualStyleBackColor = true;
            // 
            // cbOnlineVoice
            // 
            this.cbOnlineVoice.AutoSize = true;
            this.cbOnlineVoice.Checked = true;
            this.cbOnlineVoice.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbOnlineVoice.Location = new System.Drawing.Point(95, 47);
            this.cbOnlineVoice.Name = "cbOnlineVoice";
            this.cbOnlineVoice.Size = new System.Drawing.Size(132, 16);
            this.cbOnlineVoice.TabIndex = 0;
            this.cbOnlineVoice.Text = "好友上下线声音提示";
            this.cbOnlineVoice.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(314, 316);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "关于作者";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Black;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(3, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(308, 310);
            this.label4.TabIndex = 0;
            this.label4.Text = "该软件的编写,纯属锻炼自己的技术,并无任何商业目的.若有幸被你看到,但是不能得到你的认可,我在这里表示歉意,因为鄙人的技术能力有限,要是可能的话,还望提出宝贵的意" +
                "见.\r\n\r\n作者: 交个朋友呗!\r\n邮箱: 547538651@qq.com";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "选择图像文件";
            this.openFileDialog.RestoreDirectory = true;
            // 
            // cbGroupRecord
            // 
            this.cbGroupRecord.AutoSize = true;
            this.cbGroupRecord.Checked = true;
            this.cbGroupRecord.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbGroupRecord.Location = new System.Drawing.Point(95, 135);
            this.cbGroupRecord.Name = "cbGroupRecord";
            this.cbGroupRecord.Size = new System.Drawing.Size(96, 16);
            this.cbGroupRecord.TabIndex = 8;
            this.cbGroupRecord.Text = "保存群聊记录";
            this.cbGroupRecord.UseVisualStyleBackColor = true;
            // 
            // FormSystemSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(322, 342);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSystemSet";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "系统设置";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FormSystemSet_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSystemSet_FormClosing);
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbHead)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.TextBox tbLike;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbAddress;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbSex;
        private System.Windows.Forms.TextBox tbAge;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pbHead;
        private System.Windows.Forms.TextBox tbSignword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button buttonSaveOwnInfo;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.CheckBox cbOnlineVoice;
        private System.Windows.Forms.CheckBox cbChatToOneRecord;
        private System.Windows.Forms.CheckBox cbAutoRun;
        private System.Windows.Forms.CheckBox cbRockVioce;
        private System.Windows.Forms.CheckBox cbOnlineWindow;
        private System.Windows.Forms.Button buttonUse;
        private System.Windows.Forms.CheckBox cbTopmost;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox cbGroupRecord;


    }
}