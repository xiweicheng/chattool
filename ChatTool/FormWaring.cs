﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace ChatTool
{
    public partial class FormWaring : Form
    {
        private string waringType;
        private string waringMsg;
        private int time = 4000;//提示框出现的时间
        public FormWaring(string waringType, string waringMsg)
        {
            InitializeComponent();
            this.Top = SystemInformation.WorkingArea.Height - this.Height;
            this.Left = SystemInformation.WorkingArea.Width - this.Width;
            this.waringType = waringType;
            this.waringMsg = waringMsg;
        }

        private void FormWaring_Load(object sender, EventArgs e)
        {
            this.labelMsgType.Text = this.waringType;
            this.labelMsg.Text = this.waringMsg;
            this.timer.Interval = time;
            this.timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            this.Close(); 
        }
        private Point mouseDown;
        private void labelMsgType_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.mouseDown = e.Location;
            }
        }

        private void labelMsgType_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += (e.X - mouseDown.X);
                this.Top += (e.Y - mouseDown.Y);
            }
        }

        private void panel1_MouseEnter(object sender, EventArgs e)
        {
            this.timer.Stop();
        }

        private void panel1_MouseLeave(object sender, EventArgs e)
        {
            this.timer.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
