﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChatTool
{
    public class UserManager
    {
        private FormManager formManager;
        public UserManager(FormManager formManager)
        {
            this.formManager = formManager;
        }
        private List<User> listUser = new List<User>();//好友列表

        public List<User> ListUser
        {
            get { return listUser; }
            set { listUser = value; }
        }
        /// <summary>
        /// 管理用户的上下线等状态
        /// </summary>
        public void ManagerUser(User user)
        {
            if (user.LoginState == UserState.Online)//上线
            {
                foreach (User item in listUser)
                {
                    if (item.Ip == user.Ip)//已经在线
                        return;
                }
                listUser.Add(user);
                string groupName = GetFriendsGroup(user);//获取该用户所在的组名
                this.formManager.AddFriends(user, groupName);
                this.formManager.BroadcastUserOwn();//广播自己
                //每当有一个新的人上线，当本端接收到，同时要将自己组播出去
                
            }
            else if (user.LoginState == UserState.OutLine)//下线
            {
                foreach (User item in listUser)
                {
                    if (item.Ip == user.Ip)
                    {
                        listUser.Remove(item);
                        string groupName = GetFriendsGroup(user);//获取该用户所在的组名
                        this.formManager.RemoveFriends(user, groupName);
                        break;
                    }
                }
            }
            else if (user.LoginState == UserState.Hide)//隐身
            {
                foreach (User item in listUser)
                {
                    if (item.Ip == user.Ip)
                    {
                        listUser.Remove(item);
                        string groupName = GetFriendsGroup(user);//获取该用户所在的组名
                        this.formManager.RemoveFriends(user, groupName);
                        break;
                    }
                }
                this.formManager.BroadcastUserOwn();//广播自己
            }
            else if (user.LoginState == UserState.Leave)//离开
            {
                foreach (User item in listUser)
                {
                    if (item.Ip == user.Ip)
                    {
                        item.LoginState = UserState.Leave;
                        string groupName = GetFriendsGroup(user);//获取该用户所在的组名
                        this.formManager.UpdateUser(user, groupName);
                        break;
                    }
                }
                this.formManager.BroadcastUserOwn();//广播自己
            }
            else if (user.LoginState == UserState.Busy)//忙碌
            {
                foreach (User item in listUser)
                {
                    if (item.Ip == user.Ip)
                    {
                        item.LoginState = UserState.Busy;
                        string groupName = GetFriendsGroup(user);//获取该用户所在的组名
                        this.formManager.UpdateUser(user, groupName);
                        break;
                    }
                }
                this.formManager.BroadcastUserOwn();//广播自己
            }
        }
        //获取该用户所在的组名
        private string GetFriendsGroup(User user)
        {
            FriendsGroup[] groups = FileOperate.GetFriendsGroupFromFile();
            if (groups == null)
                return "我的好友";
            foreach (FriendsGroup item in groups)
            {
                if (item.IpList != null)
                {
                    foreach (string ip in item.IpList)
                    {
                        if (user.Ip == ip)
                            return item.Name;
                    }
                }
            }
            return "我的好友";
        }
        /// <summary>
        /// 加载好友组别
        /// </summary>
        public void LoadFriendsGroup()
        {
            FriendsGroup[] groups = FileOperate.GetFriendsGroupFromFile();
            this.formManager.AddGroupToTreeView(groups);
        }
    }
}
