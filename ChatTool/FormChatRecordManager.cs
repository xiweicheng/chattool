﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ChatTool
{
    public partial class FormChatRecordManager : Form
    {
        private string fileName;
        private List<ChatRecord> chatRecordList;
        public FormChatRecordManager(string fileName)
        {
            InitializeComponent();
            this.fileName = fileName;
        }

        private void FormChatRecordManager_Load(object sender, EventArgs e)
        {
            chatRecordList = FileOperate.GetChatRecordFromFile(fileName);
            if (chatRecordList != null && chatRecordList.Count != 0)
            {
                this.lbDateTime.Items.AddRange(chatRecordList.ToArray());
            }
        }

        private void lbDateTime_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.lbDateTime.SelectedIndex != -1)
            {
                ChatRecord chatRecord = this.lbDateTime.SelectedItem as ChatRecord;
                this.richTextBox1.Rtf = chatRecord.ChatContent;
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            ListBox.SelectedObjectCollection items = this.lbDateTime.SelectedItems;
            if (items != null)
            {
                for (int i = items.Count - 1; i >= 0; i--)
                {
                    chatRecordList.Remove(items[i] as ChatRecord);
                    this.lbDateTime.Items.Remove(items[i]);
                }
                this.richTextBox1.Clear();
                FileOperate.WriteChatRecordToFile(this.fileName, chatRecordList);
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            DateTime dt = dateTimePicker1.Value;
            //MessageBox.Show(dt.ToString());
            foreach (Object item in this.lbDateTime.Items)
            {
                ChatRecord cr = item as ChatRecord;
                DateTime dt1 = DateTime.Parse(cr.StartDateTime);
                if (dt1.ToShortDateString() == dt.ToShortDateString())
                {
                    this.lbDateTime.SelectedItem = item;
                    break;
                }
            }
        }
    }
}
