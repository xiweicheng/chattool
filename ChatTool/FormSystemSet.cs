﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.Win32;

namespace ChatTool
{
    public partial class FormSystemSet : Form
    {
        private FormManager formManager;
        public FormSystemSet()
        {
            InitializeComponent();
        }
        public FormSystemSet(FormManager formManager, int index)
        {
            InitializeComponent();
            this.formManager = formManager;
            this.tabControl.SelectedIndex = index;

            User userOwn = FileOperate.GetUserFromFile();
            if (userOwn != null)
            {
                this.tbName.Text = userOwn.Name;
                this.tbPassword.Text = userOwn.Password;
                this.tbSignword.Text = userOwn.SignWord;
                this.tbAge.Text = userOwn.Age;
                this.tbAddress.Text = userOwn.Address;
                this.tbLike.Text = userOwn.Like;
                this.cbSex.SelectedItem = userOwn.Sex;
                if (userOwn.Photo != null)
                    this.pbHead.Image = userOwn.Photo;
            }
            else
            {
                this.tbName.Text = Environment.UserDomainName;
                this.cbSex.SelectedIndex = 0;
                this.tbSignword.Text = "可以的话,交个朋友呗!";
            }
        }

        private void buttonSaveOwnInfo_Click(object sender, EventArgs e)
        {
            string name = tbName.Text.Trim();
            string password = tbPassword.Text.Trim();
            string signWord = tbSignword.Text.Trim();
            string sex = cbSex.SelectedItem.ToString();
            string age = tbAge.Text.Trim();
            if (age != "")
            {
                int ageInt = 20;
                bool flag = Int32.TryParse(age, out ageInt);
                if (flag == false)
                {
                    MessageBox.Show("年龄不合法!");
                    tbAge.Clear();
                    return;
                }
                if (ageInt <= 0 || ageInt > 150)
                {
                    MessageBox.Show("年龄不合法!");
                    tbAge.Clear();
                    return;
                }
            }
            string address = tbAddress.Text.Trim();
            string like = tbLike.Text.Trim();

            this.formManager.UserOwn.Name = name;
            this.formManager.UserOwn.Password = password;
            this.formManager.UserOwn.Name = name;

            this.formManager.UserOwn.SignWord = signWord;
            this.formManager.UserOwn.Sex = sex;
            this.formManager.UserOwn.Age = age;
            this.formManager.UserOwn.Address = address;
            this.formManager.UserOwn.Like = like;
            if (pbHead.Image != null)
                this.formManager.UserOwn.Photo = new Bitmap(pbHead.Image);
            this.formManager.UpdateOwnInfo();
            if (FileOperate.WriteUserToFile(this.formManager.UserOwn))
                MessageBox.Show("个人信息保存成功!");
            else
                MessageBox.Show("个人信息保存失败!");
        }

        private void pbHead_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            openFileDialog.Filter = "(图像文件)*.jpg;*.jpeg;*.bmp;*.png;*.gif|*.jpg;*.jpeg;*.bmp;*.png;*.gif";
            openFileDialog.FileName = "选择图像文件";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (Stream stream = openFileDialog.OpenFile())
                    {
                        Image image = new Bitmap(stream);
                        int width = 50;
                        int height = width * image.Height / image.Width;
                        Image newImage = new Bitmap(image, width, height);
                        if (pbHead.Image != null)
                        {
                            pbHead.Image.Dispose();
                            pbHead.Image = null;
                        }
                        this.pbHead.Image = newImage;//设置自己形象面板中的形象
                        image.Dispose();
                        image = null;
                    }
                }
                catch (Exception ee)
                {
                    MessageBox.Show("设置个人形象秀时异常: " + ee.Message);
                }
            }
        }

        private void FormSystemSet_Load(object sender, EventArgs e)
        {
            RegistryKey ms_run = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");
            if (ms_run.GetValue("AutoRunXWC") != null && ms_run.GetValue("AutoRunXWC").ToString() == Application.ExecutablePath)
                cbAutoRun.Checked = true;
            else
                cbAutoRun.Checked = false;

            if (Tool.peiZhiData.ContainsKey("LoginOrOutVoice"))
            {
                bool value = Boolean.Parse(Tool.peiZhiData["LoginOrOutVoice"]);
                this.cbOnlineVoice.Checked = value;
            }
            if (Tool.peiZhiData.ContainsKey("LoginOrOutWind"))
            {
                bool value = Boolean.Parse(Tool.peiZhiData["LoginOrOutWind"]);
                this.cbOnlineWindow.Checked = value;
            }
            if (Tool.peiZhiData.ContainsKey("RockVoice"))
            {
                bool value = Boolean.Parse(Tool.peiZhiData["RockVoice"]);
                this.cbRockVioce.Checked = value;
            }
            if (Tool.peiZhiData.ContainsKey("SaveChatToOneRecord"))
            {
                bool value = Boolean.Parse(Tool.peiZhiData["SaveChatToOneRecord"]);
                this.cbChatToOneRecord.Checked = value;
            }
            if (Tool.peiZhiData.ContainsKey("SaveChatToGroupRecord"))
            {
                bool value = Boolean.Parse(Tool.peiZhiData["SaveChatToGroupRecord"]);
                this.cbGroupRecord.Checked = value;
            }
            this.cbTopmost.Checked = Tool.isFormManagerTopmost;
        }

        private void buttonUse_Click(object sender, EventArgs e)
        {
            AddKeyValue("LoginOrOutVoice", cbOnlineVoice.Checked.ToString());
            AddKeyValue("LoginOrOutWind", cbOnlineWindow.Checked.ToString());
            AddKeyValue("RockVoice", cbRockVioce.Checked.ToString());
            AddKeyValue("SaveChatToOneRecord", cbChatToOneRecord.Checked.ToString());
            AddKeyValue("SaveChatToGroupRecord", cbGroupRecord.Checked.ToString());
            MessageBox.Show("应用成功!");
        }

        private void AddKeyValue(string key, string value)
        {
            if (Tool.peiZhiData.ContainsKey(key))
            {
                Tool.peiZhiData[key] = value;
            }
            else
                Tool.peiZhiData.Add(key, value);
        }

        private void FormSystemSet_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Tool.peiZhiData.Count != 0)
            {
                string[] pz = new string[Tool.peiZhiData.Count];
                int i = 0;
                foreach (string key in Tool.peiZhiData.Keys)
                {
                    pz[i++] = key + "=" + Tool.peiZhiData[key];
                }
                FileOperate.WritePeiZhiToFile(pz);
            }
        }

        private void cbAutoRun_CheckedChanged(object sender, EventArgs e)
        {
            if (cbAutoRun.Checked)
            {
                RegistryKey ms_run = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                ms_run.SetValue("AutoRunXWC", Application.ExecutablePath);
            }
            else
            {
                RegistryKey ms_run = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                ms_run.SetValue("AutoRunXWC", "");
            }
        }

        private void cbTopmost_CheckedChanged(object sender, EventArgs e)
        {
            this.formManager.TopMost = cbTopmost.Checked;
            Tool.isFormManagerTopmost = cbTopmost.Checked;
        }
    }
}
