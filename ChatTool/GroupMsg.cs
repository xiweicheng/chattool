﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChatTool
{
    [Serializable]
    public class GroupMsg
    {
        private string groupName;
        /// <summary>
        /// 消息从属的组名
        /// </summary>
        public string GroupName
        {
            get { return groupName; }
            set { groupName = value; }
        }
        private string groupCreator;
        /// <summary>
        /// 消息从属组的创建者
        /// </summary>
        public string GroupCreator
        {
            get { return groupCreator; }
            set { groupCreator = value; }
        }
        private string sender;
        /// <summary>
        /// 消息的发送者
        /// </summary>
        public string Sender
        {
            get { return sender; }
            set { sender = value; }
        }
        private string sendRtf;
        /// <summary>
        /// 消息的内容
        /// </summary>
        public string SendRtf
        {
            get { return sendRtf; }
            set { sendRtf = value; }
        }
    }
}
