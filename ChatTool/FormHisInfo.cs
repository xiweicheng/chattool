﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ChatTool
{
    public partial class FormHisInfo : Form
    {
        private FormChatToOne formChatToOne;
        public FormHisInfo(FormChatToOne formChatToOne)
        {
            InitializeComponent();
            this.formChatToOne = formChatToOne;
        }

        private void FormHisInfo_Load(object sender, EventArgs e)
        {
            User user = this.formChatToOne.UserClient.User;
            this.labelName.Text = user.Name;
            this.labelSign.Text = user.SignWord;
            this.labelLoginTime.Text = user.LoginDateTime;
            this.labelLikes.Text = user.Like;
            this.labelAge.Text = user.Age;
            this.labelIP.Text = user.Ip;
            this.labelPort.Text = user.ListenerPort.ToString();
            this.labelSex.Text = user.Sex;
            this.labelAddress.Text = user.Address;
            this.pbHead.Image = user.Photo;
        }
       
    }
}
