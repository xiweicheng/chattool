﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace ChatTool
{
    public partial class FormRemoteHelpDesktop : Form
    {
        private UserClient userClient;
        private FormChatToOne formChatToOne;
        public FormRemoteHelpDesktop(UserClient userClient, FormChatToOne formChatToOne)
        {
            InitializeComponent();
            this.userClient = userClient;
            this.formChatToOne = formChatToOne;
            this.Height = 600;
            this.MouseWheel += new MouseEventHandler(FormRemoteHelpDesktop_MouseWheel);
        }

        private void FormRemoteHelpDesktop_MouseWheel(object sender, MouseEventArgs e)
        {
            NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, "HelpCommand");
            NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, "MouseWheel");
            NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, e.Delta.ToString());
        }
        private delegate void DrawImageOnForm(Bitmap bitmap);
        public void DrawRemoteDesktopImage(Bitmap bitmap)
        {
            this.BeginInvoke(new DrawImageOnForm(DrawDesktopImage), bitmap);         
        }
        //在界面上绘图
        private void DrawDesktopImage(Bitmap bitmap)
        {
            using (Graphics g = this.CreateGraphics())
            {
                this.Width = (int)(this.Height * bitmap.Width / bitmap.Height);
                this.Location = new Point((Screen.AllScreens[0].Bounds.Width - this.Width) / 2, (Screen.AllScreens[0].Bounds.Height - this.Height) / 2);
                //下面的方法可以避免画面的闪烁
                Bitmap canvas = new Bitmap(this.Width, this.Height);//创建一块画布
                using (Graphics graphics = Graphics.FromImage(canvas))
                {
                    graphics.DrawImage(bitmap, 0, 0, this.Width, this.Height);//将图像绘制在画布上
                    g.DrawImage(canvas, 0, 0, this.Width, this.Height);//将画布绘制在Panel上
                    canvas.Dispose();
                }
                bitmap.Dispose();
                bitmap = null;
            }
        }
        //模拟鼠标的移动
        private void FormRemoteHelpDesktop_MouseMove(object sender, MouseEventArgs e)
        {
            NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, "HelpCommand");
            NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, "MouseMove");
            NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, e.X + "@" + e.Y + "@" + this.Height);
        }
        //鼠标按下的模拟
        private void FormRemoteHelpDesktop_MouseDown(object sender, MouseEventArgs e)
        {
            NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, "HelpCommand");
            NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, "MouseDown");
            if (e.Button == MouseButtons.Left)
                NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, "Left");
            else if (e.Button == MouseButtons.Middle)
                NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, "Middle");
            else if (e.Button == MouseButtons.Right)
                NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, "Right");
        }
        //鼠标松开的模拟
        private void FormRemoteHelpDesktop_MouseUp(object sender, MouseEventArgs e)
        {
            NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, "HelpCommand");
            NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, "MouseUp");
            if (e.Button == MouseButtons.Left)
                NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, "Left");
            else if (e.Button == MouseButtons.Middle)
                NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, "Middle");
            else if (e.Button == MouseButtons.Right)
                NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, "Right");
        }
        //鼠标进入
        private void FormRemoteHelpDesktop_MouseEnter(object sender, EventArgs e)
        {
            this.Focus();
        }
        //键盘键的按下
        private void FormRemoteHelpDesktop_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Escape)
            {
                NetManager.SendStringMsgToServer(userClient, "StopRemoteHelp");
                userClient.Close();
                this.Close();
            }
            else if (e.Control && e.KeyCode == Keys.Enter)
            {
                if (this.Height == 600)
                {
                    NetManager.SendStringMsgToServer(userClient, "FullScreen");
                    this.Location = new Point(0, 0);
                    this.Height = Screen.AllScreens[0].Bounds.Height;
                }
                else
                {
                    NetManager.SendStringMsgToServer(userClient, "PartScreen");
                    this.Height = 600;
                }
            }
        }
        //键盘键的抬起
        private void FormRemoteHelpDesktop_KeyUp(object sender, KeyEventArgs e)
        {
            NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, "HelpCommand");
            NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, "KeyPress");
            NetManager.SendStringMsgToServer(this.formChatToOne.UserClient, e.KeyValue.ToString());
        } 
        private delegate void CloseFormDelegate();
        /// <summary>
        /// 关闭窗体
        /// </summary>
        public void CloseForm()
        {
            if (this.InvokeRequired)
                this.Invoke(new CloseFormDelegate(CloseForm));
            else
            {
                userClient.Close();
                this.Close();
            }
        }

        private void FormRemoteHelpDesktop_Load(object sender, EventArgs e)
        {
            Thread receiveImageThread = new Thread(ReceiveRemoteHelpImage);
            receiveImageThread.IsBackground = true;
            receiveImageThread.Start();
        }
        /// <summary>
        /// 接收远程桌面的图片
        /// </summary>
        private void ReceiveRemoteHelpImage()
        {
            while (true)
            {
                try
                {
                    int imageLength = userClient.Br.ReadInt32();
                    byte[] bs = userClient.Br.ReadBytes(imageLength);
                    using (MemoryStream ms = new MemoryStream(bs))
                    {
                        this.DrawRemoteDesktopImage(new Bitmap(ms));
                    }
                }
                catch (Exception ee)
                {
                    MessageBox.Show("接受远程桌面图片时异常：" + ee.Message);
                    CloseForm();
                    return;
                }
               
            }
        }
    }
}
