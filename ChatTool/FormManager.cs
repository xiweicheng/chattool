﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Diagnostics;
using System.IO;

namespace ChatTool
{
    public partial class FormManager : Form
    {
        private Point mouseDown;//鼠标按下的位置
        private NetManager netManager;//网络管理

        public NetManager NetManager
        {
            get { return netManager; }
        }
        private UserManager userManager;//用户管理
        private User userOwn = new User();//一个当前自己的信息保存类
        private FormSkinSet formSkinSet;

        public User UserOwn
        {
            get { return userOwn; }
            set { userOwn = value; }
        }
        public FormManager()
        {
            InitializeComponent();
            //MessageBox.Show(true.ToString());
        }
        private void Init()
        { 
            //初始化好友列表
            userManager = new UserManager(this);
            userManager.LoadFriendsGroup();
            //开启网络监听
            netManager = new NetManager(this);//网络管理
            netManager.StartListenerThread();
            netManager.StartListenGroupMsg();
            //注册事件
            netManager.UserInfoReceived += new NetManager.UserInfoReceivedDelegate(netManager_UserInfoReceived);
        }
        private void FormManager_Load(object sender, EventArgs e)
        {
            this.labelProgromName.Text = "XWC" + DateTime.Now.Year;//设置程序名

            //初始化系统配置数据
            string[] pz = FileOperate.GetPeiZhiFromFile();
            if (pz != null)
            {
                Tool.peiZhiData.Clear();
                foreach (string item in pz)
                {
                    string[] kv = item.Split('=');
                    Tool.peiZhiData.Add(kv[0], kv[1]);
                }
            }

            //初始化用户信息
            User user = FileOperate.GetUserFromFile();
            if (user != null)
            {
                this.userOwn = user;
                this.UpdateOwnInfo();
            }
            else
            {
                //设置初始显示姓名 为本机名
                this.labelName.Text = Environment.UserDomainName;
                this.userOwn.Name = this.labelName.Text;
                this.userOwn.SignWord = this.labelMood.Text;
            }
            //初始化组别信息
            Group group1 = new Group();
            group1.Name = "全局群组";
            group1.Creator = "0.0.0.0";
            group1.CreatorName = "systemManager";
            group1.GroupTheme = "大家畅所欲言吧!";
            this.tvGroups.Nodes[0].Tag = group1;

            List<Group> listGroup = FileOperate.GroupFromFile();
            if (listGroup != null)
            {
                foreach (Group group in listGroup)
                {
                    TreeNode node = new TreeNode(group.Name);
                    node.Tag = group;
                    this.tvGroups.Nodes.Add(node);
                }
            }

            Init();//初始化
        }
       //响应有用户发来信息事件
        private void netManager_UserInfoReceived(User user)
        {
            //MessageBox.Show(user.Ip);
            userManager.ManagerUser(user);
        }
        //主面板的关闭，最小化，最大化事件响应
        private void buttonClose_Click(object sender, EventArgs e)
        {
            Button but = sender as Button;
            string butName = but.Name;
            if (butName == "buttonClose")//关闭
            {
                下线ToolStripMenuItem.PerformClick();//下线
                this.Close();
            }
            else if (butName == "buttonMax")//最大化
            {
                if (this.WindowState == FormWindowState.Normal)
                    this.WindowState = FormWindowState.Maximized;
                else
                    this.WindowState = FormWindowState.Normal;
            }
            else if (butName == "buttonMin")//最小化
            {
                this.WindowState = FormWindowState.Minimized;
            }
        }
        //控制主窗体的位置移动
        private void labelProgromName_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += (e.X - mouseDown.X);
                this.Top += (e.Y - mouseDown.Y);
            }
        }
        //记录鼠标按下的位置
        private void labelProgromName_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.mouseDown = e.Location;
            }
        }
        //设置事件 包括网络设置 个人信息设置
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            ToolStripButton but = sender as ToolStripButton;
            string butName = but.Name;
            if (butName == "tsbShowHide")
            {
                this.scMid.Panel1Collapsed = !scMid.Panel1Collapsed;
                if (but.Text == "<<")
                    but.Text = ">>";
                else
                    but.Text = "<<";
            }
            else if (butName == "tsbNetwork")
            {
                new FormSystemSet(this, 1).Show();
            }
            else if (butName == "tsbOwnInfo")
            {
                new FormSystemSet(this, 0).Show();

            }
        }
        private delegate void AddFriendsDelegate(User user, string groupName);
        /// <summary>
        /// 添加新上线好友到指定组中
        /// </summary>
        public void AddFriends(User user, string groupName)
        {
            if (tvFridents.InvokeRequired)
            {
                AddFriendsDelegate d = AddFriends;
                this.Invoke(d, user, groupName);
            }
            else
            {
                TreeNodeCollection topNodes = tvFridents.Nodes;
                foreach (TreeNode item in topNodes)
                {
                    if (item.Text == groupName)
                    {
                        TreeNode node = new TreeNode(user.Name);
                        node.Tag = user;
                        item.Nodes.Add(node);
                        if (Tool.peiZhiData.ContainsKey("LoginOrOutWind") == false || Tool.peiZhiData["LoginOrOutWind"] == "True")
                        {
                            FormWaring waring = new FormWaring("上线提示", user.Name + "上线了!");
                            waring.Show();
                        }
                        if (Tool.peiZhiData.ContainsKey("LoginOrOutVoice") == false || Tool.peiZhiData["LoginOrOutVoice"] == "True")
                        {
                            UnmanagedMemoryStream ums = Properties.Resources.Global;
                            Tool.Play(ums);
                        }
                        return;
                    }
                }
            }
        }
        /// <summary>
        /// 添加组别到TreeView
        /// </summary>
        public void AddGroupToTreeView(FriendsGroup[] groups)
        {
            if (groups != null)
            {
                foreach (FriendsGroup item in groups)
                {
                    TreeNode node = new TreeNode(item.Name);
                    ToolStripMenuItem tsm = new ToolStripMenuItem();
                    tsm.Text = item.Name;
                    tsm.Click += new EventHandler(我的好友ToolStripMenuItem_Click);
                    this.移动好友到ToolStripMenuItem.DropDownItems.Add(tsm);
                    tvFridents.Nodes.Add(node);
                }
            }
        }
        private void 我的好友ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsm = sender as ToolStripMenuItem;
            string text = tsm.Text;
            TreeNode node = tvFridents.SelectedNode;
            if (node == null)
                return;
            if (node.Parent == null)
                return;
            if (node.Parent.Text == text)
                return;

            string oldGroup = node.Parent.Text;
            string newGroup = text;
            string moveIp = ((User)node.Tag).Ip;

            //修改组文件
            FriendsGroup[] groups = FileOperate.GetFriendsGroupFromFile();
            foreach (FriendsGroup item in groups)
            {
                if (item.Name == oldGroup)
                    item.IpList.Remove(moveIp);
                if (item.Name == newGroup)
                    item.IpList.Add(moveIp);
            }
            FileOperate.WriteFriendsGroupsToFile(groups);
            //将好友从原来组中删除
            node.Parent.Nodes.Remove(node);
            //移动好友到新组
            foreach (TreeNode item in tvFridents.Nodes)
            {
                if (item.Text == newGroup)
                    item.Nodes.Add(node);
            }
        }
        private void 新建好友组ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsm = sender as ToolStripMenuItem;
            string text = tsm.Text;
            if (text == "新建好友组")
                new FormNewFriendsGroup(this).Show();
            else if (text == "删除好友组")
            {
                TreeNode node = tvFridents.SelectedNode;
                //this.移动好友到ToolStripMenuItem.DropDownItems.Add(tsm);
              
                if (node != null && node.Parent == null && node.Text != "我的好友")
                {
                    DeleteGroupByText(node.Text);
                    tvFridents.Nodes.Remove(node);
                    FriendsGroup[] groups = FileOperate.GetFriendsGroupFromFile();
                    if (groups != null && groups.Length != 0 && groups.Length != 1)
                    {
                        FriendsGroup[] groups1 = new FriendsGroup[groups.Length - 1];
                        int i = 0;
                        foreach (FriendsGroup item in groups)
                        {
                            if (item.Name == node.Text)
                                continue;
                            groups[i++] = item;
                        }
                        FileOperate.WriteFriendsGroupsToFile(groups1);
                    }
                    else
                    {
                        FileOperate.ClearFriendsGroupsToFile();
                    }
                }
            }
           
        }

        private void DeleteGroupByText(string text)
        {
            ToolStripItem tsi = null;
            foreach (ToolStripItem item in this.移动好友到ToolStripMenuItem.DropDownItems)
            {
                if (item.Text == text)
                {
                    tsi = item;
                    break;
                }
            }
            this.移动好友到ToolStripMenuItem.DropDownItems.Remove(tsi);
        }

       
        //用户状态控制
        private void buttonSetState_Click(object sender, EventArgs e)
        {
            this.cmsState.Show(this.Left + 60, this.Top + 70);
        }

        private void 上线ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsm = sender as ToolStripMenuItem;
            string text = tsm.Text;
            this.labelState.Text = "[" + text + "]";
            if (text == "上线")
            {
                userOwn.LoginState = UserState.Online;
                userOwn.LoginDateTime = DateTime.Now.ToString();
            }
            else if (text == "下线")
            {
                userOwn.LoginState = UserState.OutLine;
            }
            else if (text == "隐身")
            {
                userOwn.LoginState = UserState.Hide;
            }
            else if (text == "忙碌")
            {
                userOwn.LoginState = UserState.Busy;
            }
            else if (text == "离开")
            {
                userOwn.LoginState = UserState.Leave;
            }
            User user = new User(userOwn);//发送副本
            netManager.StartBroadcastThread(user);
        }
        private delegate void UpdateUserDelegate(User user, string groupName);
        /// <summary>
        /// 更新好友登录状态
        /// </summary>
        public void UpdateUser(User user, string groupName)
        {
            if (tvFridents.InvokeRequired)
            {
                UpdateUserDelegate d = UpdateUser;
                this.Invoke(d, user, groupName);
            }
            else
            {
                foreach (TreeNode item in tvFridents.Nodes)
                {
                    if (item.Text == groupName)
                    {
                        foreach (TreeNode item1 in item.Nodes)
                        {
                            User user1 = item1.Tag as User;
                            if (user1.Ip == user.Ip)
                            {
                                user1.LoginState = user.LoginState;
                                return;
                            }
                        }
                    }
                }
            }
        }
        private delegate void RemoveFriendsDelegate(User user, string groupName);
        /// <summary>
        /// /移除下线好友
        /// </summary>
        public void RemoveFriends(User user, string groupName)
        {
            if (tvFridents.InvokeRequired)
            {
                RemoveFriendsDelegate d = RemoveFriends;
                this.Invoke(d, user, groupName);
            }
            else
            {
                TreeNodeCollection topNodes = tvFridents.Nodes;
                foreach (TreeNode item in topNodes)
                {
                    if (item.Text == groupName)
                    {
                        TreeNodeCollection nodes = item.Nodes;
                        foreach (TreeNode item1 in nodes)
                        {
                            User user1 = item1.Tag as User;
                            if (user1.Ip == user.Ip)
                            {
                                item.Nodes.Remove(item1);
                                if (Tool.peiZhiData.ContainsKey("LoginOrOutWind") == false || Tool.peiZhiData["LoginOrOutWind"] == "True")
                                {
                                    FormWaring waring = new FormWaring("下线提示", user.Name + "下线了!");
                                    waring.Show();
                                }
                                if (Tool.peiZhiData.ContainsKey("LoginOrOutVoice") == false || Tool.peiZhiData["LoginOrOutVoice"] == "True")
                                {
                                    UnmanagedMemoryStream ums = Properties.Resources.Global;
                                    Tool.Play(ums);
                                }
                                return;
                            }
                        }
                    }
                }
            }
        }
        private List<FormChatToOne> chatToOneList = new List<FormChatToOne>();//当前一对一聊天所建立的连接集合 用于控制

        public List<FormChatToOne> ChatToOneList
        {
            get { return chatToOneList; }
        }
        
        //双击成员节点时 进行TCP连接进行一对一聊天
        private void tvFridents_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Parent == null)//这是双击的是组别节点
                return;
            User remoteUser = e.Node.Tag as User;
            foreach (FormChatToOne item in chatToOneList)
            {
                if (item.UserClient.User.Ip == remoteUser.Ip)
                    return;
            }

            UserClient userClient = netManager.ConnectToServer(remoteUser);     
            if (userClient == null)
            {
                MessageBox.Show("私聊连接建立失败！");
                return;
            }
            userClient.User = remoteUser;
            FormChatToOne chatToOne = new FormChatToOne(this, userClient);
            chatToOne.Show();
            chatToOneList.Add(chatToOne);//保存建立的连接的引用

            NetManager.SendStringMsgToServer(userClient, "chatToOne");//向对方发送要建立连接的目的
            //NetManager.SendStringMsgToServer(userClient,  GetTextByCmd("LoginName"));//向对方发送自己的姓名
            //NetManager.SendStringMsgToServer(userClient, NetManager.listenerPort.ToString());//向对方发送自己的监听端口号
        }
        private delegate void ChatToOneDelegate(UserClient userClient);
        /// <summary>
        /// 响应远程的私聊请求
        /// </summary>
        public void MyServer_ChatToOneEvent(UserClient userClient)
        {
            ChatToOneDelegate d = ExecChatToOneEvent;
            this.Invoke(d, userClient);
        }
        private void ExecChatToOneEvent(UserClient userClient)
        {
            string epStr = userClient.TcpClient.Client.RemoteEndPoint.ToString();
            string ip = epStr.Substring(0, epStr.LastIndexOf(":"));

            foreach (User item in userManager.ListUser)
            {
                if (item.Ip == ip)
                    userClient.User = item;
            }
           
            FormChatToOne chatToOne = new FormChatToOne(this, userClient);
            chatToOne.Show();
            chatToOneList.Add(chatToOne);//加入聊天窗口引用
        }
        public void MyServer_RemoteHelpEvent(UserClient userClient)
        {
            ChatToOneDelegate d = ExecRemoteHelpEvent;
            this.Invoke(d, userClient);
        }
        private void ExecRemoteHelpEvent(UserClient userClient)
        {
            FormChatToOne formChatToOne = FindFormChatToOneByUserClient(userClient);
            if (formChatToOne != null)
                formChatToOne.HandleRemoteHelpCmd(userClient);
            else
                userClient.Close();
        }
        public void MyServer_DesktopShareEvent(UserClient userClient)
        {
            ChatToOneDelegate d = ExecDesktopShareEvent;
            this.Invoke(d, userClient);
        }
        private void ExecDesktopShareEvent(UserClient userClient)
        {
            FormChatToOne formChatToOne = FindFormChatToOneByUserClient(userClient);
            if (formChatToOne != null)
                formChatToOne.HandleDesktopShareCmd(userClient);
            else
                userClient.Close();
        }
        public void MyServer_VideoTalkEvent(UserClient userClient)
        {
            ChatToOneDelegate d = ExecVideoTalkEvent;
            this.Invoke(d, userClient);
        }
        private void ExecVideoTalkEvent(UserClient userClient)
        {
            FormChatToOne formChatToOne = FindFormChatToOneByUserClient(userClient);
            if (formChatToOne != null)
                formChatToOne.HandleVideoTalkCmd(userClient);
            else
                userClient.Close();
        }
        /// <summary>
        /// 响应远程文件传输事件
        /// </summary>
        public void MyServer_TransferFileEvent(UserClient userClient)
        {
            ChatToOneDelegate d = ExecTransferFileEvent;
            this.Invoke(d, userClient);
        }
        private void ExecTransferFileEvent(UserClient userClient)
        {
            FormChatToOne formChatToOne = FindFormChatToOneByUserClient(userClient);
            if (formChatToOne != null)
                formChatToOne.HandleTransferFileCmd(userClient);
            else
                userClient.Close();
        }
        //根据IP地址找对应的私聊窗口
        private FormChatToOne FindFormChatToOneByUserClient(UserClient userClient)
        {
            string IPandPort = userClient.TcpClient.Client.RemoteEndPoint.ToString();
            string IP = IPandPort.Substring(0, IPandPort.LastIndexOf(":"));
            foreach (FormChatToOne formQQ in this.chatToOneList)
            {
                string remoteqqIPandPort = formQQ.UserClient.TcpClient.Client.RemoteEndPoint.ToString();
                string remoteqqIP = remoteqqIPandPort.Substring(0, remoteqqIPandPort.LastIndexOf(":"));
                if (remoteqqIP == IP)
                {
                    return formQQ;
                }
            }
            return null;
        }
        /// <summary>
        /// 根据命令获取信息
        /// </summary>
        public string GetTextByCmd(string cmd)
        {
            switch (cmd)
            { 
                case "LoginName":
                    return this.labelName.Text;
                default:
                    return "";
            }
        }

        private void labelProgromName_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
                this.WindowState = FormWindowState.Maximized;
            else
                this.WindowState = FormWindowState.Normal;
        }
        //个人信息设置
        private void pbOwnPicture_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            new FormSystemSet(this, 0).Show();
        }
        /// <summary>
        /// 重新设置个人信息
        /// </summary>
        public void UpdateOwnInfo()
        {
            this.labelName.Text = userOwn.Name;
            this.labelMood.Text = userOwn.SignWord;
            if (userOwn.Photo != null)
            {
                if (this.pbOwnPicture.Image != null)
                {
                    this.pbOwnPicture.Image.Dispose();
                    this.pbOwnPicture.Image = null;
                }
                this.pbOwnPicture.Image = userOwn.Photo;
            }
        }
        /// <summary>
        /// 将自己组播出去
        /// </summary>
        public void BroadcastUserOwn()
        {
            User user = new User(userOwn);//发送副本
            this.netManager.StartBroadcastThread(user);
        }

        private void tsbChangeSkin_Click(object sender, EventArgs e)
        {
            if (formSkinSet == null)
            {
                formSkinSet = new FormSkinSet(this);
                formSkinSet.Top = this.tsBottom.Top + this.Top - formSkinSet.Height;
                formSkinSet.Left = this.Left;
                formSkinSet.Show(this);
            }
            else
            {
                formSkinSet.Top = this.tsBottom.Top + this.Top - formSkinSet.Height;
                formSkinSet.Left = this.Left;
                formSkinSet.Visible = !formSkinSet.Visible;
            }
        }
        /// <summary>
        /// 设置背景皮肤图片
        /// </summary>
        public void SetBackgroudSkinImage(Image image)
        {
            this.BackgroundImage.Dispose();
            this.BackgroundImage = null;
            this.BackgroundImage = image;
        }
        /// <summary>
        /// 设置界面透明度
        /// </summary>
        public void SetFormManagerOpacity(int value)
        {
            this.Opacity = 1.0 * value / 100;
        }

        private void FormManager_Paint(object sender, PaintEventArgs e)
        {
            //Graphics g = e.Graphics;
            //g.Clear(this.BackColor);
            //g.DrawImage(Properties.Resources.skin_01, 0, 0, this.Width, this.Height);
        }

        private void tlpManager_MouseEnter(object sender, EventArgs e)
        {
            if (this.Location.Y < 0)
            {
                this.Top = 0;
                this.TopMost = Tool.isFormManagerTopmost;
                this.tlpManager.MouseEnter -= new System.EventHandler(this.tlpManager_MouseEnter);
            }
        }

        private void tlpManager_MouseLeave(object sender, EventArgs e)
        {
            if (this.Location.Y < 0)
            {
                this.Top = -this.Height + 2;
                this.TopMost = true;
                this.tlpManager.MouseEnter += new System.EventHandler(this.tlpManager_MouseEnter);
            }
        }

        private void tsbVideo_Click(object sender, EventArgs e)
        {
            ToolStripButton tsb = sender as ToolStripButton;
            string name = tsb.Name;
            try
            {
                if (name == "tsbVideo")
                {
                    process.StartInfo.FileName = FileOperate.RootDir + "\\AddFunction\\" + "桌面录制.exe";
                }
                else if (name == "tsbScreenSave")
                {
                    process.StartInfo.FileName = FileOperate.RootDir + "\\AddFunction\\" + "屏幕保护.exe";
                }
                else if (name == "tsbImageCut")
                {
                    process.StartInfo.FileName = FileOperate.RootDir + "\\AddFunction\\" + "屏幕截图.exe";
                }
                else if (name == "tsbMusic")
                {
                    process.StartInfo.FileName = FileOperate.RootDir + "\\AddFunction\\" + "音频播放.exe";
                }
                else if (name == "tsbFormControl")
                {
                    process.StartInfo.FileName = FileOperate.RootDir + "\\AddFunction\\" + "窗口速控.exe";
                }
                else if (name == "tsbVoice")
                {
                    process.StartInfo.FileName = FileOperate.RootDir + "\\AddFunction\\" + "电脑录音.exe";
                }
                process.Start();
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
          
        }

        private void 创建群组ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormGroupManager group = new FormGroupManager(this);
            group.Show();
        }
        /// <summary>
        /// 从群组视图列表中添加或者删除群组
        /// </summary>
        public void AddOrDeleteGroup(string cmd, Group newGroup)
        {
            if (cmd == "Add")
            {
                TreeNode node = new TreeNode(newGroup.Name);
                node.Tag = newGroup;
                this.tvGroups.Nodes.Add(node);
            }
            else if(cmd == "Delete")
            {
                TreeNode selectedNode = null;
                foreach (TreeNode node in this.tvGroups.Nodes)
                {
                    Group group = node.Tag as Group;
                    if (group != null && group.Name == newGroup.Name && group.Creator == group.Creator)
                    {
                        selectedNode = node;
                        break;
                    }
                }
                if (selectedNode != null)
                    this.tvGroups.Nodes.Remove(selectedNode);
            }
        }
        //群聊
        private void tvGroups_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            Group group = e.Node.Tag as Group;//此时若group为null表示全局群
            if (group.IsSelected == false)
            {
                FormChatToGroup chatToGroup = new FormChatToGroup(this, group);
                chatToGroup.Show();
            }
        }

        private void textBoxSearch_TextChanged(object sender, EventArgs e)
        {
            string text = textBoxSearch.Text.Trim();
            foreach (TreeNode item in this.tvFridents.Nodes)
            {
                foreach (TreeNode node in item.Nodes)
                {
                    if(node.Text.Contains(text))
                    {
                        if (this.tvFridents.SelectedNode != null)
                            this.tvFridents.SelectedNode.BackColor = Color.White;
                        this.tvFridents.SelectedNode = node;
                        node.Checked = true;
                        node.BackColor = Color.Red; 
                    }
                }
            }
        }

        private void textBoxSearch_MouseClick(object sender, MouseEventArgs e)
        {
            this.textBoxSearch.Clear();
            this.textBoxSearch.Focus();
        }

        private void tvFridents_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            foreach (TreeNode item in this.tvFridents.Nodes)
            {
                foreach (TreeNode node in item.Nodes)
                {
                    if (node.BackColor == Color.Red)
                        node.BackColor = Color.White;
                }
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            string pwd = this.tbPwd.Text.Trim();
            if (pwd == userOwn.Password)
            {
                this.splitContainer.Panel2Collapsed = false;
                this.splitContainer.Panel1Collapsed = true;
                this.ShowInTaskbar = true;
                this.tbPwd.Clear();
            }
            else
            {
                this.tbPwd.Clear();
                MessageBox.Show("密码输入有误!");
            }
        }

        private void tsbLock_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
                this.WindowState = FormWindowState.Normal;
            this.splitContainer.Panel1Collapsed = false;
            this.splitContainer.Panel2Collapsed = true;
            this.ShowInTaskbar = false;
        }

        private void tsbInit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("初始化后,所有好友组别信息,聊天记录都将被删除,确实要初始化吗?", "警告!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                FileOperate.InitSystem();
                MessageBox.Show("初始化完成,建立重启系统.");
            }
        }

        private void tbPwd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                this.buttonOK.PerformClick();
        }
    }
}
