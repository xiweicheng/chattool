﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using ChatTool;

namespace ChatTool
{
    public partial class FormSetCopyRegion : Form
    {
        private Point mouseDownLocation;
        private FormDrawBoard drawBorad;//画板
        private Rectangle rectDraw;//截取区域
        private FormChatToOne formChatToOne;
        private bool isCtrlDown = false;//ctrl键是否按下
        //截图区域的左上角
        public Point MouseDownLocation
        {
            get { return mouseDownLocation; }
            set { this.mouseDownLocation = value; }
        }
        private Point mouseMoveToLocation;
        //截图区域的右下角
        public Point MouseMoveToLocation
        {
            get { return mouseMoveToLocation; }
        }
        public FormSetCopyRegion()
        {
            InitializeComponent();
        }
        public FormSetCopyRegion(FormChatToOne formChatToOne)
        {
            InitializeComponent();
            this.formChatToOne = formChatToOne;
        }
        private FormChatToGroup formChatToGroup;
        public FormSetCopyRegion(FormChatToGroup formChatToGroup)
        {
            InitializeComponent();
            this.formChatToGroup = formChatToGroup;
        }
        private void FormSetCopyRegion_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (e.X > mouseDownLocation.X && e.Y > mouseDownLocation.Y)
                    rectDraw = new Rectangle(mouseDownLocation, new Size(e.X - mouseDownLocation.X, e.Y - mouseDownLocation.Y));
                else if (e.X > mouseDownLocation.X && e.Y < mouseDownLocation.Y)
                    rectDraw = new Rectangle(mouseDownLocation.X, e.Y, e.X - mouseDownLocation.X, mouseDownLocation.Y - e.Y);
                else if (e.X < mouseDownLocation.X && e.Y < mouseDownLocation.Y)
                    rectDraw = new Rectangle(e.X, e.Y, mouseDownLocation.X - e.X, mouseDownLocation.Y - e.Y);
                else
                    rectDraw = new Rectangle(e.X, mouseDownLocation.Y, mouseDownLocation.X - e.X, e.Y - mouseDownLocation.Y);
                drawBorad.Draw(true, rectDraw, e);
            }
            else
                drawBorad.Draw(false, rectDraw, e);
        }
        private void FormSetCopyRegion_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.mouseDownLocation = e.Location;
            }
        }
        private void FormSetCopyRegion_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (this.isCtrlDown == true)
                    return;
                this.mouseDownLocation = rectDraw.Location;
                this.mouseMoveToLocation = new Point(rectDraw.X + rectDraw.Width, rectDraw.Y + rectDraw.Height);
                if (mouseMoveToLocation.X > mouseDownLocation.X && mouseMoveToLocation.Y > mouseDownLocation.Y)
                {
                    drawBorad.Close();
                    this.Visible = false;
                    Bitmap bitmap = PrintScreen.CopySelectedRegion(mouseDownLocation, mouseMoveToLocation);
                    if (formChatToOne != null)
                    {
                        formChatToOne.Visible = formChatToOne.Visible == false ? true : true;
                        formChatToOne.HandleBitmap(bitmap);
                    }
                    else if (formChatToGroup != null)
                    {
                        formChatToGroup.Visible = formChatToGroup.Visible == false ? true : true;
                        formChatToGroup.HandleBitmap(bitmap);
                    }
                    this.Close();
                }
            }
        }
        private void FormSetCopyRegion_Load(object sender, EventArgs e)
        {
            drawBorad = new FormDrawBoard();//画板
            drawBorad.Show();
        }

        private void FormSetCopyRegion_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
                this.isCtrlDown = true;
        }

        private void FormSetCopyRegion_KeyUp(object sender, KeyEventArgs e)
        {
             this.isCtrlDown = false;
        }
    }
}
