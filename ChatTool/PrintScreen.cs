﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing;

namespace ChatTool
{
    public class PrintScreen
    {
        //区域截图
        public static Bitmap CopySelectedRegion(Point mouseDownLocation, Point mouseMoveToLocation)
        {
            Bitmap desktopImage = new Bitmap(mouseMoveToLocation.X - mouseDownLocation.X, mouseMoveToLocation.Y - mouseDownLocation.Y);
            using (Graphics g = Graphics.FromImage(desktopImage))
            {
                g.CopyFromScreen(mouseDownLocation, new Point(0, 0), desktopImage.Size);
            }
            return desktopImage;
        }
    }   
}
