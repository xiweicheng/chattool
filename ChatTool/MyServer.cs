﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;

namespace ChatTool
{
    public class MyServer
    {
        private int port;//监听端口
        private TcpListener listener;//监听器
        private FormManager formManager;//主界面引用
        //private bool isNormalExit = false;//是不是正常退出接受数据的线程
        public MyServer(int port, FormManager formManager)
        {
            this.port = port;
            this.formManager = formManager;
            this.ChatToOneEvent += new ChatToOneDelegate(this.formManager.MyServer_ChatToOneEvent);
            this.TransferFileEvent += new ChatToOneDelegate(this.formManager.MyServer_TransferFileEvent);
            this.VideoTalkEvent += new ChatToOneDelegate(this.formManager.MyServer_VideoTalkEvent);
            this.DesktopShareEvent += new ChatToOneDelegate(this.formManager.MyServer_DesktopShareEvent);
            this.RemoteHelpEvent += new ChatToOneDelegate(this.formManager.MyServer_RemoteHelpEvent);
        }
        /// <summary>
        /// 开启监听
        /// </summary>
        public void StartListen()
        {
            listener = new TcpListener(NetManager.GetHostIP(), port);
            listener.Start();
            Thread threadListener = new Thread(ListenConnect);
            threadListener.IsBackground = true;
            threadListener.Start(); 
        }
        /// <summary>
        /// 监听线程方法体
        /// </summary>
        private void ListenConnect()
        {
            TcpClient tcpClient = null;
            while (true)
            {
                try
                {
                    tcpClient = listener.AcceptTcpClient();//阻塞性方法
                }
                catch (Exception)//当停止监听 或者 退出窗体时 AcceptTcpClient() 会产生异常 因此可以 退出循环
                {
                    return;
                }
                UserClient userClient = new UserClient(tcpClient);
                SelectHandle(userClient);//选择处理方式
            }
        }
        public delegate void ChatToOneDelegate(UserClient userClient);
        public event ChatToOneDelegate ChatToOneEvent;
        public event ChatToOneDelegate TransferFileEvent;
        public event ChatToOneDelegate VideoTalkEvent;
        public event ChatToOneDelegate DesktopShareEvent;
        public event ChatToOneDelegate RemoteHelpEvent; 
        //选择处理方式
        private void SelectHandle(UserClient userClient)
        {
            //读取客户端的请求命令
            string command = userClient.Br.ReadString().ToLower();
            //命令解析
            switch (command)
            {
                    //一对一聊天 
                case "chattoone":
                    if (this.ChatToOneEvent != null)
                        ChatToOneEvent(userClient);
                    break;
                    //文件传送
                case "transferfile":
                    if (this.TransferFileEvent != null)
                        TransferFileEvent(userClient);
                    break;
                    //视频聊天
                case "videotalk":
                    if (VideoTalkEvent != null)
                        VideoTalkEvent(userClient);
                    break;
                    //桌面共享
                case "desktopshare":
                    if (DesktopShareEvent != null)
                        DesktopShareEvent(userClient);
                    break;
                    //远程协助
                case "remotehelp":
                    if (RemoteHelpEvent != null)
                        RemoteHelpEvent(userClient);
                    break;
                default:
                    break;
            }
        }
    }
}
