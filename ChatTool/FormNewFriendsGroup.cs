﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ChatTool
{
    public partial class FormNewFriendsGroup : Form
    {
        private FormManager formManager;
        public FormNewFriendsGroup()
        {
            InitializeComponent();
        }
        public FormNewFriendsGroup(FormManager formManager)
        {
            InitializeComponent();
            this.formManager = formManager;
        }
        private void buttonOK_Click(object sender, EventArgs e)
        {
            string name = textBox.Text.Trim();
            if (name.Length == 0)
                return;
            FriendsGroup[] groups = FileOperate.GetFriendsGroupFromFile();
            if (groups != null)
            {
                foreach (FriendsGroup item in groups)
                {
                    if (item.Name == name)
                    {
                        MessageBox.Show("该名称的组别已经存在,请重新命名.");
                        textBox.SelectAll();
                        textBox.Focus();
                        return;
                    }
                }
                FriendsGroup[] newGroups = new FriendsGroup[groups.Length + 1];
                Array.Copy(groups, 0, newGroups, 0, groups.Length);
                FriendsGroup newGroup = new FriendsGroup(name);
                newGroups[groups.Length] = newGroup;
                FileOperate.WriteFriendsGroupsToFile(newGroups);
                this.formManager.AddGroupToTreeView(new FriendsGroup[1] { newGroup });
            }
            else
            {
                FriendsGroup[] newGroups = new FriendsGroup[1];
                newGroups[0] = new FriendsGroup(name);
                FileOperate.WriteFriendsGroupsToFile(newGroups);
                this.formManager.AddGroupToTreeView(newGroups);
            }
            this.Close();
        }
    }
}
