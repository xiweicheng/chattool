﻿namespace ChatTool
{
    partial class FormRemoteHelpDesktop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRemoteHelpDesktop));
            this.SuspendLayout();
            // 
            // FormRemoteHelpDesktop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(960, 600);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormRemoteHelpDesktop";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "远程协助界面(Ctrl+E退出)";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FormRemoteHelpDesktop_Load);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FormRemoteHelpDesktop_MouseUp);
            this.MouseEnter += new System.EventHandler(this.FormRemoteHelpDesktop_MouseEnter);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormRemoteHelpDesktop_MouseDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormRemoteHelpDesktop_KeyUp);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FormRemoteHelpDesktop_MouseMove);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormRemoteHelpDesktop_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion


    }
}