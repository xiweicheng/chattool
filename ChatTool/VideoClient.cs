﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;

namespace ChatTool
{
    class VideoClient
    {
        private FormVideoControl formVideoControl;
        private UserClient userClient;
        private bool isExit = false;
        private Cvideo cVideo;
        public VideoClient(FormVideoControl formVideoControl, Cvideo cVideo)
        {
            this.formVideoControl = formVideoControl;
            this.cVideo = cVideo;
        }
        public void StartConnectServer()
        {
            Thread connectThread = new Thread(ConnectServer);
            connectThread.IsBackground = true;
            connectThread.Start();
        }
        private void ConnectServer()
        {
            TcpClient client = new TcpClient(AddressFamily.InterNetwork);
            try
            {
                IPEndPoint ipe = new IPEndPoint(IPAddress.Parse(formVideoControl.FormChatToOne.UserClient.User.Ip), formVideoControl.FormChatToOne.UserClient.User.ListenerPort);
                client.Connect(ipe);//连接服务器
            }
            catch
            {
                client.Close();
                MessageBox.Show("连接服务器失败,可能对方服务器未开启.");
                return;
            }
            formVideoControl.SetButtonText("buttonVideoTalk", "停止视聊");

            userClient = new UserClient(client);
            Thread receiveThread = new Thread(ReceiveServerData);
            receiveThread.IsBackground = true;
            receiveThread.Start();

            //这边一切就绪后 向对方发送聊天请求
            NetManager.SendStringMsgToServer(userClient, "VideoTalk");  
        }
        private void ReceiveServerData()
        {
            while (isExit == false)
            {
                try
                {
                    string command = userClient.Br.ReadString().ToLower();
                    switch (command.ToLower())
                    {
                        case "refuse":
                            formVideoControl.SetButtonText("buttonVideoTalk", "视频聊天");
                            CloseVideoClient();
                            MessageBox.Show("对方拒绝了你的视频聊天请求.");
                            return;
                        case "agree":
                            formVideoControl.StartSendImage(userClient);
                            break;
                        case "stop":
                            formVideoControl.SetButtonText("buttonVideoTalk", "视频聊天");
                            CloseVideoClient();
                            MessageBox.Show("对方停止了与你的视频聊天.");
                            break;
                        default:
                            break;
                    }
                }
                catch//可能 本端自己关的连接 这时 isExit=true 而 userClient也已经关闭 所以执行 userClient.br.ReadString();会出现异常 而被捕获
                {
                    if (isExit == false)//这时对方先关闭连接的情况 也就是对方的电脑突然关闭 而没有向我方发送"logout"命令
                    {
                        userClient.Close();
                        MessageBox.Show("与对方的视频聊天连接断开了.");
                    }
                    formVideoControl.SetButtonText("buttonVideoTalk", "视频聊天");
                    break;
                }
            }
        }

        public void CloseVideoClient()
        {
            isExit = true;
            userClient.Close();
        }
    }
}
