﻿namespace ChatTool
{
    partial class FormGroupManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tbSaySome = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbOwnGroup = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonDeleteGroup = new System.Windows.Forms.Button();
            this.buttonAddNewGroup = new System.Windows.Forms.Button();
            this.tbNewGroupName = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonOut = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lbHaveJoinGroup = new System.Windows.Forms.ListBox();
            this.buttonJoin = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbNetGroup = new System.Windows.Forms.ListBox();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(286, 309);
            this.tabControl.TabIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(278, 283);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "自建群组管理";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tbSaySome);
            this.groupBox5.Location = new System.Drawing.Point(14, 143);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(253, 50);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "建群寄语";
            // 
            // tbSaySome
            // 
            this.tbSaySome.Location = new System.Drawing.Point(7, 21);
            this.tbSaySome.Name = "tbSaySome";
            this.tbSaySome.Size = new System.Drawing.Size(240, 21);
            this.tbSaySome.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbOwnGroup);
            this.groupBox1.Location = new System.Drawing.Point(11, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(259, 131);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "已创建群组";
            // 
            // lbOwnGroup
            // 
            this.lbOwnGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbOwnGroup.FormattingEnabled = true;
            this.lbOwnGroup.HorizontalScrollbar = true;
            this.lbOwnGroup.ItemHeight = 12;
            this.lbOwnGroup.Location = new System.Drawing.Point(3, 17);
            this.lbOwnGroup.Name = "lbOwnGroup";
            this.lbOwnGroup.Size = new System.Drawing.Size(253, 100);
            this.lbOwnGroup.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonDeleteGroup);
            this.groupBox2.Controls.Add(this.buttonAddNewGroup);
            this.groupBox2.Controls.Add(this.tbNewGroupName);
            this.groupBox2.Location = new System.Drawing.Point(14, 199);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(253, 78);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "新群组名称";
            // 
            // buttonDeleteGroup
            // 
            this.buttonDeleteGroup.Location = new System.Drawing.Point(135, 47);
            this.buttonDeleteGroup.Name = "buttonDeleteGroup";
            this.buttonDeleteGroup.Size = new System.Drawing.Size(94, 23);
            this.buttonDeleteGroup.TabIndex = 2;
            this.buttonDeleteGroup.Text = "删除选定群组";
            this.buttonDeleteGroup.UseVisualStyleBackColor = true;
            this.buttonDeleteGroup.Click += new System.EventHandler(this.buttonDeleteGroup_Click);
            // 
            // buttonAddNewGroup
            // 
            this.buttonAddNewGroup.Location = new System.Drawing.Point(24, 47);
            this.buttonAddNewGroup.Name = "buttonAddNewGroup";
            this.buttonAddNewGroup.Size = new System.Drawing.Size(89, 23);
            this.buttonAddNewGroup.TabIndex = 1;
            this.buttonAddNewGroup.Text = "加入新建群组";
            this.buttonAddNewGroup.UseVisualStyleBackColor = true;
            this.buttonAddNewGroup.Click += new System.EventHandler(this.buttonAddNewGroup_Click);
            // 
            // tbNewGroupName
            // 
            this.tbNewGroupName.Location = new System.Drawing.Point(6, 20);
            this.tbNewGroupName.Name = "tbNewGroupName";
            this.tbNewGroupName.Size = new System.Drawing.Size(241, 21);
            this.tbNewGroupName.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.buttonOut);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.buttonJoin);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(278, 283);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "网络群组管理";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // buttonOut
            // 
            this.buttonOut.Location = new System.Drawing.Point(149, 246);
            this.buttonOut.Name = "buttonOut";
            this.buttonOut.Size = new System.Drawing.Size(93, 29);
            this.buttonOut.TabIndex = 3;
            this.buttonOut.Text = "退出加入群组";
            this.buttonOut.UseVisualStyleBackColor = true;
            this.buttonOut.Click += new System.EventHandler(this.buttonOut_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lbHaveJoinGroup);
            this.groupBox4.Location = new System.Drawing.Point(9, 140);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(263, 100);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "已经加入的网络群组";
            // 
            // lbHaveJoinGroup
            // 
            this.lbHaveJoinGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbHaveJoinGroup.FormattingEnabled = true;
            this.lbHaveJoinGroup.HorizontalScrollbar = true;
            this.lbHaveJoinGroup.ItemHeight = 12;
            this.lbHaveJoinGroup.Location = new System.Drawing.Point(3, 17);
            this.lbHaveJoinGroup.Name = "lbHaveJoinGroup";
            this.lbHaveJoinGroup.Size = new System.Drawing.Size(257, 76);
            this.lbHaveJoinGroup.TabIndex = 0;
            // 
            // buttonJoin
            // 
            this.buttonJoin.Location = new System.Drawing.Point(30, 246);
            this.buttonJoin.Name = "buttonJoin";
            this.buttonJoin.Size = new System.Drawing.Size(93, 29);
            this.buttonJoin.TabIndex = 1;
            this.buttonJoin.Text = "加入选定群组";
            this.buttonJoin.UseVisualStyleBackColor = true;
            this.buttonJoin.Click += new System.EventHandler(this.buttonJoin_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbNetGroup);
            this.groupBox3.Location = new System.Drawing.Point(9, 7);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(263, 127);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "当前网络上的群组";
            // 
            // lbNetGroup
            // 
            this.lbNetGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbNetGroup.FormattingEnabled = true;
            this.lbNetGroup.HorizontalScrollbar = true;
            this.lbNetGroup.ItemHeight = 12;
            this.lbNetGroup.Location = new System.Drawing.Point(3, 17);
            this.lbNetGroup.Name = "lbNetGroup";
            this.lbNetGroup.Size = new System.Drawing.Size(257, 100);
            this.lbNetGroup.TabIndex = 0;
            // 
            // FormGroupManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(286, 309);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormGroupManager";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "群组管理";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FormGroupManager_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormGroupManager_FormClosing);
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox lbOwnGroup;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbNewGroupName;
        private System.Windows.Forms.Button buttonDeleteGroup;
        private System.Windows.Forms.Button buttonAddNewGroup;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox lbNetGroup;
        private System.Windows.Forms.Button buttonJoin;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ListBox lbHaveJoinGroup;
        private System.Windows.Forms.Button buttonOut;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tbSaySome;
    }
}