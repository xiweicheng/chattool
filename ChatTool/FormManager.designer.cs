﻿namespace ChatTool
{
    partial class FormManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormManager));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("我的好友");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("全局群组");
            this.tlpManager = new System.Windows.Forms.TableLayoutPanel();
            this.tlpHead = new System.Windows.Forms.TableLayoutPanel();
            this.labelProgromName = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonMax = new System.Windows.Forms.Button();
            this.buttonMin = new System.Windows.Forms.Button();
            this.tlpLoginInfo = new System.Windows.Forms.TableLayoutPanel();
            this.pbOwnPicture = new System.Windows.Forms.PictureBox();
            this.panelState = new System.Windows.Forms.Panel();
            this.labelMood = new System.Windows.Forms.Label();
            this.labelState = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.buttonSetState = new System.Windows.Forms.Button();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.tsBottom = new System.Windows.Forms.ToolStrip();
            this.tsbShowHide = new System.Windows.Forms.ToolStripButton();
            this.tsbNetwork = new System.Windows.Forms.ToolStripButton();
            this.tsbOwnInfo = new System.Windows.Forms.ToolStripButton();
            this.tsbChangeSkin = new System.Windows.Forms.ToolStripButton();
            this.scMid = new System.Windows.Forms.SplitContainer();
            this.tsSide = new System.Windows.Forms.ToolStrip();
            this.tsbVideo = new System.Windows.Forms.ToolStripButton();
            this.tsbScreenSave = new System.Windows.Forms.ToolStripButton();
            this.tsbImageCut = new System.Windows.Forms.ToolStripButton();
            this.tsbMusic = new System.Windows.Forms.ToolStripButton();
            this.tsbFormControl = new System.Windows.Forms.ToolStripButton();
            this.tsbInit = new System.Windows.Forms.ToolStripButton();
            this.tsbVoice = new System.Windows.Forms.ToolStripButton();
            this.tsbLock = new System.Windows.Forms.ToolStripButton();
            this.tcManager = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tvFridents = new System.Windows.Forms.TreeView();
            this.cmsFriends = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.新建好友组ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除好友组ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.移动好友到ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.我的好友ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tvGroups = new System.Windows.Forms.TreeView();
            this.cmsGroup = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.创建群组ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.cmsState = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.上线ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.下线ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.隐身ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.忙碌ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.离开ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.process = new System.Diagnostics.Process();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonOK = new System.Windows.Forms.Button();
            this.tbPwd = new System.Windows.Forms.TextBox();
            this.tlpManager.SuspendLayout();
            this.tlpHead.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tlpLoginInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbOwnPicture)).BeginInit();
            this.panelState.SuspendLayout();
            this.tsBottom.SuspendLayout();
            this.scMid.Panel1.SuspendLayout();
            this.scMid.Panel2.SuspendLayout();
            this.scMid.SuspendLayout();
            this.tsSide.SuspendLayout();
            this.tcManager.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.cmsFriends.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.cmsGroup.SuspendLayout();
            this.cmsState.SuspendLayout();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpManager
            // 
            this.tlpManager.BackColor = System.Drawing.Color.Transparent;
            this.tlpManager.ColumnCount = 1;
            this.tlpManager.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpManager.Controls.Add(this.tlpHead, 0, 0);
            this.tlpManager.Controls.Add(this.tlpLoginInfo, 0, 1);
            this.tlpManager.Controls.Add(this.textBoxSearch, 0, 2);
            this.tlpManager.Controls.Add(this.tsBottom, 0, 4);
            this.tlpManager.Controls.Add(this.scMid, 0, 3);
            this.tlpManager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpManager.Location = new System.Drawing.Point(0, 0);
            this.tlpManager.Name = "tlpManager";
            this.tlpManager.RowCount = 5;
            this.tlpManager.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tlpManager.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tlpManager.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tlpManager.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpManager.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 63F));
            this.tlpManager.Size = new System.Drawing.Size(255, 537);
            this.tlpManager.TabIndex = 0;
            this.tlpManager.MouseLeave += new System.EventHandler(this.tlpManager_MouseLeave);
            this.tlpManager.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseMove);
            this.tlpManager.MouseDown += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseDown);
            // 
            // tlpHead
            // 
            this.tlpHead.BackColor = System.Drawing.Color.Transparent;
            this.tlpHead.ColumnCount = 2;
            this.tlpHead.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpHead.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 89F));
            this.tlpHead.Controls.Add(this.labelProgromName, 0, 0);
            this.tlpHead.Controls.Add(this.panel1, 1, 0);
            this.tlpHead.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpHead.Location = new System.Drawing.Point(3, 3);
            this.tlpHead.Name = "tlpHead";
            this.tlpHead.RowCount = 1;
            this.tlpHead.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpHead.Size = new System.Drawing.Size(249, 31);
            this.tlpHead.TabIndex = 0;
            // 
            // labelProgromName
            // 
            this.labelProgromName.BackColor = System.Drawing.Color.Transparent;
            this.labelProgromName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelProgromName.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelProgromName.Location = new System.Drawing.Point(3, 0);
            this.labelProgromName.Name = "labelProgromName";
            this.labelProgromName.Size = new System.Drawing.Size(154, 31);
            this.labelProgromName.TabIndex = 0;
            this.labelProgromName.Text = "XWC";
            this.labelProgromName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelProgromName.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseMove);
            this.labelProgromName.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseDoubleClick);
            this.labelProgromName.MouseDown += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseDown);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonClose);
            this.panel1.Controls.Add(this.buttonMax);
            this.panel1.Controls.Add(this.buttonMin);
            this.panel1.Location = new System.Drawing.Point(163, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(83, 25);
            this.panel1.TabIndex = 0;
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.BackgroundImage = global::ChatTool.Properties.Resources.CloseBtnNormal;
            this.buttonClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonClose.Location = new System.Drawing.Point(56, 0);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(27, 23);
            this.buttonClose.TabIndex = 2;
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonMax
            // 
            this.buttonMax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMax.BackgroundImage = global::ChatTool.Properties.Resources.MaxBtnNormal;
            this.buttonMax.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonMax.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonMax.Location = new System.Drawing.Point(29, 0);
            this.buttonMax.Name = "buttonMax";
            this.buttonMax.Size = new System.Drawing.Size(28, 23);
            this.buttonMax.TabIndex = 1;
            this.buttonMax.UseVisualStyleBackColor = true;
            this.buttonMax.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonMin
            // 
            this.buttonMin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMin.BackgroundImage = global::ChatTool.Properties.Resources.MinBtnNormal;
            this.buttonMin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonMin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonMin.Location = new System.Drawing.Point(2, 0);
            this.buttonMin.Name = "buttonMin";
            this.buttonMin.Size = new System.Drawing.Size(28, 23);
            this.buttonMin.TabIndex = 0;
            this.buttonMin.UseVisualStyleBackColor = true;
            this.buttonMin.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // tlpLoginInfo
            // 
            this.tlpLoginInfo.ColumnCount = 2;
            this.tlpLoginInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tlpLoginInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLoginInfo.Controls.Add(this.pbOwnPicture, 0, 0);
            this.tlpLoginInfo.Controls.Add(this.panelState, 1, 0);
            this.tlpLoginInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpLoginInfo.Location = new System.Drawing.Point(3, 40);
            this.tlpLoginInfo.Name = "tlpLoginInfo";
            this.tlpLoginInfo.RowCount = 1;
            this.tlpLoginInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLoginInfo.Size = new System.Drawing.Size(249, 55);
            this.tlpLoginInfo.TabIndex = 1;
            // 
            // pbOwnPicture
            // 
            this.pbOwnPicture.BackColor = System.Drawing.Color.White;
            this.pbOwnPicture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbOwnPicture.Location = new System.Drawing.Point(3, 3);
            this.pbOwnPicture.Name = "pbOwnPicture";
            this.pbOwnPicture.Size = new System.Drawing.Size(46, 49);
            this.pbOwnPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbOwnPicture.TabIndex = 0;
            this.pbOwnPicture.TabStop = false;
            this.toolTip.SetToolTip(this.pbOwnPicture, "双击击设置个人信息");
            this.pbOwnPicture.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pbOwnPicture_MouseDoubleClick);
            // 
            // panelState
            // 
            this.panelState.Controls.Add(this.labelMood);
            this.panelState.Controls.Add(this.labelState);
            this.panelState.Controls.Add(this.labelName);
            this.panelState.Controls.Add(this.buttonSetState);
            this.panelState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelState.Location = new System.Drawing.Point(55, 3);
            this.panelState.Name = "panelState";
            this.panelState.Size = new System.Drawing.Size(191, 49);
            this.panelState.TabIndex = 1;
            this.panelState.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseMove);
            this.panelState.MouseDown += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseDown);
            // 
            // labelMood
            // 
            this.labelMood.AutoSize = true;
            this.labelMood.BackColor = System.Drawing.Color.Transparent;
            this.labelMood.Location = new System.Drawing.Point(6, 27);
            this.labelMood.Name = "labelMood";
            this.labelMood.Size = new System.Drawing.Size(275, 12);
            this.labelMood.TabIndex = 4;
            this.labelMood.Text = "人生需要快乐，更需要朋友；没有朋友就没有快乐.";
            this.labelMood.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseMove);
            this.labelMood.MouseDown += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseDown);
            // 
            // labelState
            // 
            this.labelState.AutoSize = true;
            this.labelState.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelState.Location = new System.Drawing.Point(148, 4);
            this.labelState.Name = "labelState";
            this.labelState.Size = new System.Drawing.Size(49, 14);
            this.labelState.TabIndex = 3;
            this.labelState.Text = "[离线]";
            this.labelState.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseMove);
            this.labelState.MouseDown += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseDown);
            // 
            // labelName
            // 
            this.labelName.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelName.Location = new System.Drawing.Point(37, 3);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(112, 23);
            this.labelName.TabIndex = 2;
            this.labelName.Text = "交个朋友呗！";
            this.labelName.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseMove);
            this.labelName.MouseDown += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseDown);
            // 
            // buttonSetState
            // 
            this.buttonSetState.BackgroundImage = global::ChatTool.Properties.Resources.status;
            this.buttonSetState.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonSetState.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSetState.Location = new System.Drawing.Point(6, 0);
            this.buttonSetState.Name = "buttonSetState";
            this.buttonSetState.Size = new System.Drawing.Size(26, 24);
            this.buttonSetState.TabIndex = 1;
            this.toolTip.SetToolTip(this.buttonSetState, "用户状态管理");
            this.buttonSetState.UseVisualStyleBackColor = true;
            this.buttonSetState.Click += new System.EventHandler(this.buttonSetState_Click);
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxSearch.Location = new System.Drawing.Point(3, 101);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(249, 21);
            this.textBoxSearch.TabIndex = 2;
            this.textBoxSearch.Text = "搜好友";
            this.textBoxSearch.TextChanged += new System.EventHandler(this.textBoxSearch_TextChanged);
            this.textBoxSearch.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBoxSearch_MouseClick);
            // 
            // tsBottom
            // 
            this.tsBottom.BackColor = System.Drawing.Color.Transparent;
            this.tsBottom.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsBottom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbShowHide,
            this.tsbNetwork,
            this.tsbOwnInfo,
            this.tsbChangeSkin});
            this.tsBottom.Location = new System.Drawing.Point(0, 474);
            this.tsBottom.Name = "tsBottom";
            this.tsBottom.Size = new System.Drawing.Size(255, 25);
            this.tsBottom.TabIndex = 4;
            this.tsBottom.Text = "toolStrip1";
            // 
            // tsbShowHide
            // 
            this.tsbShowHide.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbShowHide.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowHide.Image")));
            this.tsbShowHide.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowHide.Name = "tsbShowHide";
            this.tsbShowHide.Size = new System.Drawing.Size(30, 22);
            this.tsbShowHide.Text = "<<";
            this.tsbShowHide.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // tsbNetwork
            // 
            this.tsbNetwork.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbNetwork.Image = ((System.Drawing.Image)(resources.GetObject("tsbNetwork.Image")));
            this.tsbNetwork.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNetwork.Name = "tsbNetwork";
            this.tsbNetwork.Size = new System.Drawing.Size(60, 22);
            this.tsbNetwork.Text = "系统设置";
            this.tsbNetwork.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // tsbOwnInfo
            // 
            this.tsbOwnInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbOwnInfo.Image = ((System.Drawing.Image)(resources.GetObject("tsbOwnInfo.Image")));
            this.tsbOwnInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbOwnInfo.Name = "tsbOwnInfo";
            this.tsbOwnInfo.Size = new System.Drawing.Size(60, 22);
            this.tsbOwnInfo.Text = "个人信息";
            this.tsbOwnInfo.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // tsbChangeSkin
            // 
            this.tsbChangeSkin.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbChangeSkin.Image = ((System.Drawing.Image)(resources.GetObject("tsbChangeSkin.Image")));
            this.tsbChangeSkin.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbChangeSkin.Name = "tsbChangeSkin";
            this.tsbChangeSkin.Size = new System.Drawing.Size(60, 22);
            this.tsbChangeSkin.Text = "更换皮肤";
            this.tsbChangeSkin.Click += new System.EventHandler(this.tsbChangeSkin_Click);
            // 
            // scMid
            // 
            this.scMid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scMid.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.scMid.Location = new System.Drawing.Point(3, 127);
            this.scMid.Name = "scMid";
            // 
            // scMid.Panel1
            // 
            this.scMid.Panel1.Controls.Add(this.tsSide);
            // 
            // scMid.Panel2
            // 
            this.scMid.Panel2.Controls.Add(this.tcManager);
            this.scMid.Size = new System.Drawing.Size(249, 344);
            this.scMid.SplitterDistance = 32;
            this.scMid.TabIndex = 5;
            // 
            // tsSide
            // 
            this.tsSide.BackColor = System.Drawing.Color.Transparent;
            this.tsSide.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsSide.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsSide.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.tsSide.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbVideo,
            this.tsbScreenSave,
            this.tsbImageCut,
            this.tsbMusic,
            this.tsbFormControl,
            this.tsbInit,
            this.tsbVoice,
            this.tsbLock});
            this.tsSide.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.tsSide.Location = new System.Drawing.Point(0, 0);
            this.tsSide.Name = "tsSide";
            this.tsSide.Size = new System.Drawing.Size(32, 344);
            this.tsSide.TabIndex = 0;
            this.tsSide.Text = "toolStrip1";
            // 
            // tsbVideo
            // 
            this.tsbVideo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbVideo.Image = global::ChatTool.Properties.Resources.Video1;
            this.tsbVideo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbVideo.Name = "tsbVideo";
            this.tsbVideo.Size = new System.Drawing.Size(30, 28);
            this.tsbVideo.Text = "桌面录制";
            this.tsbVideo.Click += new System.EventHandler(this.tsbVideo_Click);
            // 
            // tsbScreenSave
            // 
            this.tsbScreenSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbScreenSave.Image = global::ChatTool.Properties.Resources.ScreenSave;
            this.tsbScreenSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbScreenSave.Name = "tsbScreenSave";
            this.tsbScreenSave.Size = new System.Drawing.Size(30, 28);
            this.tsbScreenSave.Text = "屏幕保护";
            this.tsbScreenSave.Click += new System.EventHandler(this.tsbVideo_Click);
            // 
            // tsbImageCut
            // 
            this.tsbImageCut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbImageCut.Image = global::ChatTool.Properties.Resources.ImageCut;
            this.tsbImageCut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbImageCut.Name = "tsbImageCut";
            this.tsbImageCut.Size = new System.Drawing.Size(30, 28);
            this.tsbImageCut.Text = "屏幕截图";
            this.tsbImageCut.Click += new System.EventHandler(this.tsbVideo_Click);
            // 
            // tsbMusic
            // 
            this.tsbMusic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMusic.Image = global::ChatTool.Properties.Resources.Music;
            this.tsbMusic.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMusic.Name = "tsbMusic";
            this.tsbMusic.Size = new System.Drawing.Size(30, 28);
            this.tsbMusic.Text = "音乐播放";
            this.tsbMusic.Click += new System.EventHandler(this.tsbVideo_Click);
            // 
            // tsbFormControl
            // 
            this.tsbFormControl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbFormControl.Image = global::ChatTool.Properties.Resources.FormControl;
            this.tsbFormControl.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbFormControl.Name = "tsbFormControl";
            this.tsbFormControl.Size = new System.Drawing.Size(30, 28);
            this.tsbFormControl.Text = "窗口速控";
            this.tsbFormControl.Click += new System.EventHandler(this.tsbVideo_Click);
            // 
            // tsbInit
            // 
            this.tsbInit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbInit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbInit.Image = global::ChatTool.Properties.Resources.Empty;
            this.tsbInit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbInit.Name = "tsbInit";
            this.tsbInit.Size = new System.Drawing.Size(30, 28);
            this.tsbInit.Text = "系统初始化";
            this.tsbInit.Click += new System.EventHandler(this.tsbInit_Click);
            // 
            // tsbVoice
            // 
            this.tsbVoice.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbVoice.Image = global::ChatTool.Properties.Resources._1474_Tape;
            this.tsbVoice.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbVoice.Name = "tsbVoice";
            this.tsbVoice.Size = new System.Drawing.Size(30, 28);
            this.tsbVoice.Text = "电脑录音";
            this.tsbVoice.Click += new System.EventHandler(this.tsbVideo_Click);
            // 
            // tsbLock
            // 
            this.tsbLock.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbLock.Image = global::ChatTool.Properties.Resources.LOCK;
            this.tsbLock.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLock.Name = "tsbLock";
            this.tsbLock.Size = new System.Drawing.Size(30, 28);
            this.tsbLock.Text = "界面锁定";
            this.tsbLock.Click += new System.EventHandler(this.tsbLock_Click);
            // 
            // tcManager
            // 
            this.tcManager.Controls.Add(this.tabPage1);
            this.tcManager.Controls.Add(this.tabPage2);
            this.tcManager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcManager.Location = new System.Drawing.Point(0, 0);
            this.tcManager.Name = "tcManager";
            this.tcManager.SelectedIndex = 0;
            this.tcManager.Size = new System.Drawing.Size(213, 344);
            this.tcManager.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tvFridents);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(205, 318);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "好友";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tvFridents
            // 
            this.tvFridents.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tvFridents.ContextMenuStrip = this.cmsFriends;
            this.tvFridents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvFridents.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tvFridents.Location = new System.Drawing.Point(3, 3);
            this.tvFridents.Name = "tvFridents";
            treeNode1.Name = "Node0";
            treeNode1.Text = "我的好友";
            this.tvFridents.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.tvFridents.Size = new System.Drawing.Size(199, 312);
            this.tvFridents.TabIndex = 0;
            this.tvFridents.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvFridents_NodeMouseDoubleClick);
            this.tvFridents.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvFridents_NodeMouseClick);
            // 
            // cmsFriends
            // 
            this.cmsFriends.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.新建好友组ToolStripMenuItem,
            this.删除好友组ToolStripMenuItem,
            this.移动好友到ToolStripMenuItem});
            this.cmsFriends.Name = "cmsFriends";
            this.cmsFriends.Size = new System.Drawing.Size(137, 70);
            // 
            // 新建好友组ToolStripMenuItem
            // 
            this.新建好友组ToolStripMenuItem.Name = "新建好友组ToolStripMenuItem";
            this.新建好友组ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.新建好友组ToolStripMenuItem.Text = "新建好友组";
            this.新建好友组ToolStripMenuItem.Click += new System.EventHandler(this.新建好友组ToolStripMenuItem_Click);
            // 
            // 删除好友组ToolStripMenuItem
            // 
            this.删除好友组ToolStripMenuItem.Name = "删除好友组ToolStripMenuItem";
            this.删除好友组ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.删除好友组ToolStripMenuItem.Text = "删除好友组";
            this.删除好友组ToolStripMenuItem.Click += new System.EventHandler(this.新建好友组ToolStripMenuItem_Click);
            // 
            // 移动好友到ToolStripMenuItem
            // 
            this.移动好友到ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.我的好友ToolStripMenuItem});
            this.移动好友到ToolStripMenuItem.Name = "移动好友到ToolStripMenuItem";
            this.移动好友到ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.移动好友到ToolStripMenuItem.Text = "移动好友到";
            // 
            // 我的好友ToolStripMenuItem
            // 
            this.我的好友ToolStripMenuItem.Name = "我的好友ToolStripMenuItem";
            this.我的好友ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.我的好友ToolStripMenuItem.Text = "我的好友";
            this.我的好友ToolStripMenuItem.Click += new System.EventHandler(this.我的好友ToolStripMenuItem_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tvGroups);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(205, 318);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "群组";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tvGroups
            // 
            this.tvGroups.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tvGroups.ContextMenuStrip = this.cmsGroup;
            this.tvGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvGroups.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tvGroups.ImageIndex = 1;
            this.tvGroups.ImageList = this.imageList;
            this.tvGroups.Location = new System.Drawing.Point(3, 3);
            this.tvGroups.Name = "tvGroups";
            treeNode2.Name = "节点0";
            treeNode2.Text = "全局群组";
            this.tvGroups.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode2});
            this.tvGroups.SelectedImageIndex = 1;
            this.tvGroups.ShowLines = false;
            this.tvGroups.Size = new System.Drawing.Size(199, 312);
            this.tvGroups.TabIndex = 1;
            this.tvGroups.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvGroups_NodeMouseDoubleClick);
            // 
            // cmsGroup
            // 
            this.cmsGroup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.创建群组ToolStripMenuItem});
            this.cmsGroup.Name = "cmsGroup";
            this.cmsGroup.Size = new System.Drawing.Size(125, 26);
            // 
            // 创建群组ToolStripMenuItem
            // 
            this.创建群组ToolStripMenuItem.Name = "创建群组ToolStripMenuItem";
            this.创建群组ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.创建群组ToolStripMenuItem.Text = "群组管理";
            this.创建群组ToolStripMenuItem.Click += new System.EventHandler(this.创建群组ToolStripMenuItem_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "user.ico");
            this.imageList.Images.SetKeyName(1, "users.ico");
            // 
            // cmsState
            // 
            this.cmsState.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.上线ToolStripMenuItem,
            this.下线ToolStripMenuItem,
            this.隐身ToolStripMenuItem,
            this.忙碌ToolStripMenuItem,
            this.离开ToolStripMenuItem});
            this.cmsState.Name = "cmsState";
            this.cmsState.Size = new System.Drawing.Size(101, 114);
            // 
            // 上线ToolStripMenuItem
            // 
            this.上线ToolStripMenuItem.Name = "上线ToolStripMenuItem";
            this.上线ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.上线ToolStripMenuItem.Text = "上线";
            this.上线ToolStripMenuItem.Click += new System.EventHandler(this.上线ToolStripMenuItem_Click);
            // 
            // 下线ToolStripMenuItem
            // 
            this.下线ToolStripMenuItem.Name = "下线ToolStripMenuItem";
            this.下线ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.下线ToolStripMenuItem.Text = "下线";
            this.下线ToolStripMenuItem.Click += new System.EventHandler(this.上线ToolStripMenuItem_Click);
            // 
            // 隐身ToolStripMenuItem
            // 
            this.隐身ToolStripMenuItem.Name = "隐身ToolStripMenuItem";
            this.隐身ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.隐身ToolStripMenuItem.Text = "隐身";
            this.隐身ToolStripMenuItem.Click += new System.EventHandler(this.上线ToolStripMenuItem_Click);
            // 
            // 忙碌ToolStripMenuItem
            // 
            this.忙碌ToolStripMenuItem.Name = "忙碌ToolStripMenuItem";
            this.忙碌ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.忙碌ToolStripMenuItem.Text = "忙碌";
            this.忙碌ToolStripMenuItem.Click += new System.EventHandler(this.上线ToolStripMenuItem_Click);
            // 
            // 离开ToolStripMenuItem
            // 
            this.离开ToolStripMenuItem.Name = "离开ToolStripMenuItem";
            this.离开ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.离开ToolStripMenuItem.Text = "离开";
            this.离开ToolStripMenuItem.Click += new System.EventHandler(this.上线ToolStripMenuItem_Click);
            // 
            // process
            // 
            this.process.StartInfo.Domain = "";
            this.process.StartInfo.LoadUserProfile = false;
            this.process.StartInfo.Password = null;
            this.process.StartInfo.StandardErrorEncoding = null;
            this.process.StartInfo.StandardOutputEncoding = null;
            this.process.StartInfo.UserName = "";
            this.process.SynchronizingObject = this;
            // 
            // splitContainer
            // 
            this.splitContainer.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer.Panel1.MouseLeave += new System.EventHandler(this.tlpManager_MouseLeave);
            this.splitContainer.Panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseMove);
            this.splitContainer.Panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseDown);
            this.splitContainer.Panel1.MouseEnter += new System.EventHandler(this.tlpManager_MouseEnter);
            this.splitContainer.Panel1Collapsed = true;
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.tlpManager);
            this.splitContainer.Size = new System.Drawing.Size(255, 537);
            this.splitContainer.SplitterDistance = 225;
            this.splitContainer.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Location = new System.Drawing.Point(37, 208);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(186, 109);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "界面解锁";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonOK);
            this.panel2.Controls.Add(this.tbPwd);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 17);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(180, 89);
            this.panel2.TabIndex = 0;
            this.panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseMove);
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.labelProgromName_MouseDown);
            // 
            // buttonOK
            // 
            this.buttonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOK.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.buttonOK.Location = new System.Drawing.Point(63, 53);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(56, 25);
            this.buttonOK.TabIndex = 1;
            this.buttonOK.Text = "解锁";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // tbPwd
            // 
            this.tbPwd.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbPwd.Location = new System.Drawing.Point(19, 16);
            this.tbPwd.Name = "tbPwd";
            this.tbPwd.PasswordChar = '*';
            this.tbPwd.Size = new System.Drawing.Size(142, 23);
            this.tbPwd.TabIndex = 0;
            this.tbPwd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbPwd_KeyDown);
            // 
            // FormManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lime;
            this.BackgroundImage = global::ChatTool.Properties.Resources.skin_01;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(255, 537);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainer);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "聊天管理界面";
            this.Load += new System.EventHandler(this.FormManager_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FormManager_Paint);
            this.tlpManager.ResumeLayout(false);
            this.tlpManager.PerformLayout();
            this.tlpHead.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tlpLoginInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbOwnPicture)).EndInit();
            this.panelState.ResumeLayout(false);
            this.panelState.PerformLayout();
            this.tsBottom.ResumeLayout(false);
            this.tsBottom.PerformLayout();
            this.scMid.Panel1.ResumeLayout(false);
            this.scMid.Panel1.PerformLayout();
            this.scMid.Panel2.ResumeLayout(false);
            this.scMid.ResumeLayout(false);
            this.tsSide.ResumeLayout(false);
            this.tsSide.PerformLayout();
            this.tcManager.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.cmsFriends.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.cmsGroup.ResumeLayout(false);
            this.cmsState.ResumeLayout(false);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpManager;
        private System.Windows.Forms.TableLayoutPanel tlpHead;
        private System.Windows.Forms.Label labelProgromName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonMax;
        private System.Windows.Forms.Button buttonMin;
        private System.Windows.Forms.TableLayoutPanel tlpLoginInfo;
        private System.Windows.Forms.PictureBox pbOwnPicture;
        private System.Windows.Forms.Panel panelState;
        private System.Windows.Forms.Label labelMood;
        private System.Windows.Forms.Label labelState;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Button buttonSetState;
        private System.Windows.Forms.TextBox textBoxSearch;
        private System.Windows.Forms.ToolStrip tsBottom;
        private System.Windows.Forms.ToolStripButton tsbShowHide;
        private System.Windows.Forms.ToolStripButton tsbNetwork;
        private System.Windows.Forms.ToolStripButton tsbOwnInfo;
        private System.Windows.Forms.SplitContainer scMid;
        private System.Windows.Forms.TabControl tcManager;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TreeView tvFridents;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TreeView tvGroups;
        private System.Windows.Forms.ContextMenuStrip cmsFriends;
        private System.Windows.Forms.ToolStripMenuItem 新建好友组ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除好友组ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmsState;
        private System.Windows.Forms.ToolStripMenuItem 上线ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 下线ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 隐身ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 忙碌ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 离开ToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ToolStripMenuItem 移动好友到ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 我的好友ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton tsbChangeSkin;
        private System.Windows.Forms.ToolStrip tsSide;
        private System.Windows.Forms.ToolStripButton tsbVideo;
        private System.Windows.Forms.ToolStripButton tsbScreenSave;
        private System.Windows.Forms.ToolStripButton tsbImageCut;
        private System.Windows.Forms.ToolStripButton tsbMusic;
        private System.Windows.Forms.ToolStripButton tsbFormControl;
        private System.Diagnostics.Process process;
        private System.Windows.Forms.ContextMenuStrip cmsGroup;
        private System.Windows.Forms.ToolStripMenuItem 创建群组ToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.TextBox tbPwd;
        private System.Windows.Forms.ToolStripButton tsbLock;
        private System.Windows.Forms.ToolStripButton tsbInit;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripButton tsbVoice;


    }
}

