﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FYL.BusinessLogic;
using System.Net;
using System.Windows.Forms;

namespace ChatTool
{
    public class VoiceClient
    {
        private NetChat netChat;
        private int chatPort = 59892;//语音聊天的监听端口号
        private FormChatToOne formChatToOne;
        public VoiceClient(FormChatToOne formChatToOne)
        {
            this.formChatToOne = formChatToOne;
        }
        public void InitVoiceClient()
        {
            netChat = new NetChat(chatPort);
            IPEndPoint ipe = new IPEndPoint(NetManager.GetHostIP(), chatPort);
            netChat.BindSelf(ipe);
        }
        public void BeginVoiceTalk()
        {
            try
            {
                //MessageBox.Show("ssdsdsd");
                netChat.SetRemoteIPEnd(formChatToOne.UserClient.User.Ip, chatPort);
                netChat.Intptr = formChatToOne.Handle;
                netChat.InitVoice();
                netChat.Listen();
                netChat.StartSendVoice();
                MessageBox.Show("语聊已开启!");

            }
            catch (Exception ee)
            {
                MessageBox.Show("语聊失败,异常:" + ee.Message);
            }
        }
        public void StopVoiceTalk()
        {
            netChat.Stop();
        }
    }
}
