﻿namespace ChatTool
{
    partial class FormChatToGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormChatToGroup));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.pbHisPhoto = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonMax = new System.Windows.Forms.Button();
            this.buttonMin = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelMood = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.tsUp = new System.Windows.Forms.ToolStrip();
            this.tsbFileTransfer = new System.Windows.Forms.ToolStripButton();
            this.tsbDesktopShare = new System.Windows.Forms.ToolStripButton();
            this.tsbVoiceChat = new System.Windows.Forms.ToolStripButton();
            this.tsbVideoChat = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tsMid = new System.Windows.Forms.ToolStrip();
            this.tsbSetFont = new System.Windows.Forms.ToolStripButton();
            this.tsbFaceManager = new System.Windows.Forms.ToolStripButton();
            this.tsbSendPicture = new System.Windows.Forms.ToolStripButton();
            this.tsbMsgRecord = new System.Windows.Forms.ToolStripButton();
            this.ttssbSendCutPicture = new System.Windows.Forms.ToolStripSplitButton();
            this.截图时隐藏聊天窗口ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tsDown = new System.Windows.Forms.ToolStrip();
            this.tsbSend = new System.Windows.Forms.ToolStripSplitButton();
            this.按Enter发送消息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.按CtrlEnter发送消息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbClose = new System.Windows.Forms.ToolStripButton();
            this.rtbWrite = new System.Windows.Forms.RichTextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rtbRecord = new System.Windows.Forms.RichTextBox();
            this.tsFont = new System.Windows.Forms.ToolStrip();
            this.tscbFontFamily = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tscbFontSize = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbAddRough = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbIncline = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbUnderline = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbStrikeout = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbFontColor = new System.Windows.Forms.ToolStripButton();
            this.gbMembers = new System.Windows.Forms.GroupBox();
            this.tvMembers = new System.Windows.Forms.TreeView();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHisPhoto)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tsUp.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tsMid.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tsDown.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tsFont.SuspendLayout();
            this.gbMembers.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tsUp, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.splitContainer1, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(567, 465);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel2.Controls.Add(this.pbHisPhoto, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel1, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(561, 45);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // pbHisPhoto
            // 
            this.pbHisPhoto.BackColor = System.Drawing.Color.White;
            this.pbHisPhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbHisPhoto.Image = global::ChatTool.Properties.Resources.Applications_MSN;
            this.pbHisPhoto.Location = new System.Drawing.Point(3, 3);
            this.pbHisPhoto.Name = "pbHisPhoto";
            this.pbHisPhoto.Size = new System.Drawing.Size(42, 39);
            this.pbHisPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbHisPhoto.TabIndex = 1;
            this.pbHisPhoto.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonClose);
            this.panel1.Controls.Add(this.buttonMax);
            this.panel1.Controls.Add(this.buttonMin);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(474, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(84, 39);
            this.panel1.TabIndex = 2;
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseMove);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.BackgroundImage = global::ChatTool.Properties.Resources.CloseBtnNormal;
            this.buttonClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonClose.Location = new System.Drawing.Point(55, 0);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(27, 23);
            this.buttonClose.TabIndex = 8;
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonMin_Click);
            // 
            // buttonMax
            // 
            this.buttonMax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMax.BackgroundImage = global::ChatTool.Properties.Resources.MaxBtnNormal;
            this.buttonMax.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonMax.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonMax.Location = new System.Drawing.Point(28, 0);
            this.buttonMax.Name = "buttonMax";
            this.buttonMax.Size = new System.Drawing.Size(28, 23);
            this.buttonMax.TabIndex = 7;
            this.buttonMax.UseVisualStyleBackColor = true;
            this.buttonMax.Click += new System.EventHandler(this.buttonMin_Click);
            // 
            // buttonMin
            // 
            this.buttonMin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMin.BackgroundImage = global::ChatTool.Properties.Resources.MinBtnNormal;
            this.buttonMin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonMin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonMin.Location = new System.Drawing.Point(1, 0);
            this.buttonMin.Name = "buttonMin";
            this.buttonMin.Size = new System.Drawing.Size(28, 23);
            this.buttonMin.TabIndex = 6;
            this.buttonMin.UseVisualStyleBackColor = true;
            this.buttonMin.Click += new System.EventHandler(this.buttonMin_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.labelMood);
            this.panel2.Controls.Add(this.labelName);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(51, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(417, 39);
            this.panel2.TabIndex = 3;
            this.panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseMove);
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            // 
            // labelMood
            // 
            this.labelMood.AutoSize = true;
            this.labelMood.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelMood.Location = new System.Drawing.Point(3, 22);
            this.labelMood.Name = "labelMood";
            this.labelMood.Size = new System.Drawing.Size(168, 14);
            this.labelMood.TabIndex = 5;
            this.labelMood.Text = "无聊的时候，想想我们吧!";
            this.labelMood.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseMove);
            this.labelMood.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelName.Location = new System.Drawing.Point(3, 3);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(90, 14);
            this.labelName.TabIndex = 4;
            this.labelName.Text = "交个朋友呗!";
            this.labelName.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseMove);
            this.labelName.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            // 
            // tsUp
            // 
            this.tsUp.AutoSize = false;
            this.tsUp.BackColor = System.Drawing.Color.Transparent;
            this.tsUp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsUp.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsUp.ImageScalingSize = new System.Drawing.Size(30, 30);
            this.tsUp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbFileTransfer,
            this.tsbDesktopShare,
            this.tsbVoiceChat,
            this.tsbVideoChat});
            this.tsUp.Location = new System.Drawing.Point(0, 51);
            this.tsUp.Name = "tsUp";
            this.tsUp.Size = new System.Drawing.Size(567, 38);
            this.tsUp.TabIndex = 1;
            this.tsUp.Text = "toolStrip1";
            this.tsUp.Visible = false;
            this.tsUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            this.tsUp.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseMove);
            // 
            // tsbFileTransfer
            // 
            this.tsbFileTransfer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbFileTransfer.Image = global::ChatTool.Properties.Resources.FileTransfer;
            this.tsbFileTransfer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbFileTransfer.Name = "tsbFileTransfer";
            this.tsbFileTransfer.Size = new System.Drawing.Size(34, 35);
            this.tsbFileTransfer.Text = "文件传送";
            // 
            // tsbDesktopShare
            // 
            this.tsbDesktopShare.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbDesktopShare.Image = global::ChatTool.Properties.Resources.DesktopShare;
            this.tsbDesktopShare.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDesktopShare.Name = "tsbDesktopShare";
            this.tsbDesktopShare.Size = new System.Drawing.Size(34, 35);
            this.tsbDesktopShare.Text = "桌面共享";
            // 
            // tsbVoiceChat
            // 
            this.tsbVoiceChat.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbVoiceChat.Image = global::ChatTool.Properties.Resources.Vioce;
            this.tsbVoiceChat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbVoiceChat.Name = "tsbVoiceChat";
            this.tsbVoiceChat.Size = new System.Drawing.Size(34, 35);
            this.tsbVoiceChat.Text = "语音聊天";
            // 
            // tsbVideoChat
            // 
            this.tsbVideoChat.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbVideoChat.Image = global::ChatTool.Properties.Resources.Video;
            this.tsbVideoChat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbVideoChat.Name = "tsbVideoChat";
            this.tsbVideoChat.Size = new System.Drawing.Size(34, 35);
            this.tsbVideoChat.Text = "视频聊天";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(3, 92);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gbMembers);
            this.splitContainer1.Size = new System.Drawing.Size(561, 370);
            this.splitContainer1.SplitterDistance = 388;
            this.splitContainer1.TabIndex = 2;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tsMid, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.rtbWrite, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(388, 370);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // tsMid
            // 
            this.tsMid.BackColor = System.Drawing.Color.Transparent;
            this.tsMid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsMid.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMid.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.tsMid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbSetFont,
            this.tsbFaceManager,
            this.tsbSendPicture,
            this.tsbMsgRecord,
            this.ttssbSendCutPicture});
            this.tsMid.Location = new System.Drawing.Point(0, 209);
            this.tsMid.Name = "tsMid";
            this.tsMid.Size = new System.Drawing.Size(388, 28);
            this.tsMid.TabIndex = 2;
            this.tsMid.Text = "toolStrip2";
            // 
            // tsbSetFont
            // 
            this.tsbSetFont.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbSetFont.Image = global::ChatTool.Properties.Resources.FontDialog;
            this.tsbSetFont.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSetFont.Name = "tsbSetFont";
            this.tsbSetFont.Size = new System.Drawing.Size(24, 25);
            this.tsbSetFont.Text = "设置字体颜色和格式";
            this.tsbSetFont.Click += new System.EventHandler(this.tsbSetFont_Click);
            // 
            // tsbFaceManager
            // 
            this.tsbFaceManager.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbFaceManager.Image = global::ChatTool.Properties.Resources.Face;
            this.tsbFaceManager.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbFaceManager.Name = "tsbFaceManager";
            this.tsbFaceManager.Size = new System.Drawing.Size(24, 25);
            this.tsbFaceManager.Text = "常用表情";
            this.tsbFaceManager.Click += new System.EventHandler(this.tsbFaceManager_Click);
            // 
            // tsbSendPicture
            // 
            this.tsbSendPicture.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbSendPicture.Image = global::ChatTool.Properties.Resources.SendPicture;
            this.tsbSendPicture.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSendPicture.Name = "tsbSendPicture";
            this.tsbSendPicture.Size = new System.Drawing.Size(24, 25);
            this.tsbSendPicture.Text = "发送图片";
            this.tsbSendPicture.Click += new System.EventHandler(this.tsbSendPicture_Click);
            // 
            // tsbMsgRecord
            // 
            this.tsbMsgRecord.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbMsgRecord.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbMsgRecord.Image = ((System.Drawing.Image)(resources.GetObject("tsbMsgRecord.Image")));
            this.tsbMsgRecord.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMsgRecord.Name = "tsbMsgRecord";
            this.tsbMsgRecord.Size = new System.Drawing.Size(60, 25);
            this.tsbMsgRecord.Text = "消息记录";
            this.tsbMsgRecord.Click += new System.EventHandler(this.tsbMsgRecord_Click);
            // 
            // ttssbSendCutPicture
            // 
            this.ttssbSendCutPicture.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ttssbSendCutPicture.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.截图时隐藏聊天窗口ToolStripMenuItem});
            this.ttssbSendCutPicture.Image = global::ChatTool.Properties.Resources.SendcutPicture;
            this.ttssbSendCutPicture.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ttssbSendCutPicture.Name = "ttssbSendCutPicture";
            this.ttssbSendCutPicture.Size = new System.Drawing.Size(36, 25);
            this.ttssbSendCutPicture.Text = "发送屏幕截图";
            this.ttssbSendCutPicture.ButtonClick += new System.EventHandler(this.ttssbSendCutPicture_ButtonClick);
            // 
            // 截图时隐藏聊天窗口ToolStripMenuItem
            // 
            this.截图时隐藏聊天窗口ToolStripMenuItem.Name = "截图时隐藏聊天窗口ToolStripMenuItem";
            this.截图时隐藏聊天窗口ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.截图时隐藏聊天窗口ToolStripMenuItem.Text = "截图时隐藏聊天窗口";
            this.截图时隐藏聊天窗口ToolStripMenuItem.Click += new System.EventHandler(this.截图时隐藏聊天窗口ToolStripMenuItem_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 123F));
            this.tableLayoutPanel4.Controls.Add(this.tsDown, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 340);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(382, 27);
            this.tableLayoutPanel4.TabIndex = 3;
            this.tableLayoutPanel4.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseMove);
            this.tableLayoutPanel4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            // 
            // tsDown
            // 
            this.tsDown.BackColor = System.Drawing.Color.Transparent;
            this.tsDown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsDown.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsDown.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbSend,
            this.toolStripSeparator1,
            this.tsbClose});
            this.tsDown.Location = new System.Drawing.Point(259, 0);
            this.tsDown.Name = "tsDown";
            this.tsDown.Size = new System.Drawing.Size(123, 27);
            this.tsDown.TabIndex = 0;
            this.tsDown.Text = "toolStrip3";
            // 
            // tsbSend
            // 
            this.tsbSend.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbSend.AutoSize = false;
            this.tsbSend.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbSend.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.按Enter发送消息ToolStripMenuItem,
            this.按CtrlEnter发送消息ToolStripMenuItem});
            this.tsbSend.Image = ((System.Drawing.Image)(resources.GetObject("tsbSend.Image")));
            this.tsbSend.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSend.Name = "tsbSend";
            this.tsbSend.Size = new System.Drawing.Size(50, 22);
            this.tsbSend.Text = "发送";
            this.tsbSend.ButtonClick += new System.EventHandler(this.tsbSend_ButtonClick);
            this.tsbSend.MouseEnter += new System.EventHandler(this.tsbSend_MouseEnter);
            // 
            // 按Enter发送消息ToolStripMenuItem
            // 
            this.按Enter发送消息ToolStripMenuItem.Checked = true;
            this.按Enter发送消息ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.按Enter发送消息ToolStripMenuItem.Enabled = false;
            this.按Enter发送消息ToolStripMenuItem.Name = "按Enter发送消息ToolStripMenuItem";
            this.按Enter发送消息ToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.按Enter发送消息ToolStripMenuItem.Text = "按Enter发送消息";
            this.按Enter发送消息ToolStripMenuItem.Click += new System.EventHandler(this.按Enter发送消息ToolStripMenuItem_Click);
            // 
            // 按CtrlEnter发送消息ToolStripMenuItem
            // 
            this.按CtrlEnter发送消息ToolStripMenuItem.Name = "按CtrlEnter发送消息ToolStripMenuItem";
            this.按CtrlEnter发送消息ToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.按CtrlEnter发送消息ToolStripMenuItem.Text = "按Ctrl+Enter发送消息";
            this.按CtrlEnter发送消息ToolStripMenuItem.Click += new System.EventHandler(this.按Enter发送消息ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator1.AutoSize = false;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(16, 27);
            // 
            // tsbClose
            // 
            this.tsbClose.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbClose.AutoSize = false;
            this.tsbClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbClose.Image = ((System.Drawing.Image)(resources.GetObject("tsbClose.Image")));
            this.tsbClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbClose.Name = "tsbClose";
            this.tsbClose.Size = new System.Drawing.Size(50, 22);
            this.tsbClose.Text = "清空";
            this.tsbClose.Click += new System.EventHandler(this.tsbClose_Click);
            // 
            // rtbWrite
            // 
            this.rtbWrite.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbWrite.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbWrite.Location = new System.Drawing.Point(3, 240);
            this.rtbWrite.Name = "rtbWrite";
            this.rtbWrite.Size = new System.Drawing.Size(382, 94);
            this.rtbWrite.TabIndex = 1;
            this.rtbWrite.Text = "";
            this.rtbWrite.WordWrap = false;
            this.rtbWrite.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rtbWrite_KeyDown);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.rtbRecord);
            this.panel3.Controls.Add(this.tsFont);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(382, 203);
            this.panel3.TabIndex = 4;
            // 
            // rtbRecord
            // 
            this.rtbRecord.BackColor = System.Drawing.Color.White;
            this.rtbRecord.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbRecord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbRecord.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rtbRecord.Location = new System.Drawing.Point(0, 0);
            this.rtbRecord.Name = "rtbRecord";
            this.rtbRecord.ReadOnly = true;
            this.rtbRecord.Size = new System.Drawing.Size(382, 203);
            this.rtbRecord.TabIndex = 1;
            this.rtbRecord.Text = "";
            this.rtbRecord.WordWrap = false;
            // 
            // tsFont
            // 
            this.tsFont.BackColor = System.Drawing.Color.Transparent;
            this.tsFont.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tsFont.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsFont.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.tsFont.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tscbFontFamily,
            this.toolStripSeparator2,
            this.tscbFontSize,
            this.toolStripSeparator6,
            this.tsbAddRough,
            this.toolStripSeparator3,
            this.tsbIncline,
            this.toolStripSeparator4,
            this.tsbUnderline,
            this.toolStripSeparator7,
            this.tsbStrikeout,
            this.toolStripSeparator5,
            this.tsbFontColor});
            this.tsFont.Location = new System.Drawing.Point(0, 136);
            this.tsFont.Margin = new System.Windows.Forms.Padding(1);
            this.tsFont.Name = "tsFont";
            this.tsFont.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsFont.Size = new System.Drawing.Size(376, 27);
            this.tsFont.TabIndex = 0;
            this.tsFont.Text = "toolStrip4";
            this.tsFont.Visible = false;
            // 
            // tscbFontFamily
            // 
            this.tscbFontFamily.AutoSize = false;
            this.tscbFontFamily.DropDownHeight = 100;
            this.tscbFontFamily.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tscbFontFamily.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.tscbFontFamily.IntegralHeight = false;
            this.tscbFontFamily.MaxDropDownItems = 6;
            this.tscbFontFamily.Name = "tscbFontFamily";
            this.tscbFontFamily.Size = new System.Drawing.Size(100, 25);
            this.tscbFontFamily.SelectedIndexChanged += new System.EventHandler(this.tscbFontFamily_SelectedIndexChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.AutoSize = false;
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(16, 25);
            // 
            // tscbFontSize
            // 
            this.tscbFontSize.AutoSize = false;
            this.tscbFontSize.DropDownHeight = 100;
            this.tscbFontSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tscbFontSize.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.tscbFontSize.IntegralHeight = false;
            this.tscbFontSize.Items.AddRange(new object[] {
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22"});
            this.tscbFontSize.Name = "tscbFontSize";
            this.tscbFontSize.Size = new System.Drawing.Size(50, 25);
            this.tscbFontSize.SelectedIndexChanged += new System.EventHandler(this.tscbFontSize_SelectedIndexChanged);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.AutoSize = false;
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(10, 25);
            // 
            // tsbAddRough
            // 
            this.tsbAddRough.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAddRough.Image = global::ChatTool.Properties.Resources.Bold;
            this.tsbAddRough.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAddRough.Name = "tsbAddRough";
            this.tsbAddRough.Size = new System.Drawing.Size(24, 24);
            this.tsbAddRough.Text = "加粗";
            this.tsbAddRough.Click += new System.EventHandler(this.tsbSetFont_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.AutoSize = false;
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(10, 25);
            // 
            // tsbIncline
            // 
            this.tsbIncline.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbIncline.Image = global::ChatTool.Properties.Resources.Italic;
            this.tsbIncline.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbIncline.Name = "tsbIncline";
            this.tsbIncline.Size = new System.Drawing.Size(24, 24);
            this.tsbIncline.Text = "倾斜";
            this.tsbIncline.Click += new System.EventHandler(this.tsbSetFont_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.AutoSize = false;
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(10, 25);
            // 
            // tsbUnderline
            // 
            this.tsbUnderline.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbUnderline.Image = global::ChatTool.Properties.Resources.Underline;
            this.tsbUnderline.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbUnderline.Name = "tsbUnderline";
            this.tsbUnderline.Size = new System.Drawing.Size(24, 24);
            this.tsbUnderline.Text = "下划线";
            this.tsbUnderline.Click += new System.EventHandler(this.tsbSetFont_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.AutoSize = false;
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(10, 25);
            // 
            // tsbStrikeout
            // 
            this.tsbStrikeout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbStrikeout.Image = global::ChatTool.Properties.Resources.Underline;
            this.tsbStrikeout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbStrikeout.Name = "tsbStrikeout";
            this.tsbStrikeout.Size = new System.Drawing.Size(24, 24);
            this.tsbStrikeout.Text = "中划线";
            this.tsbStrikeout.Click += new System.EventHandler(this.tsbSetFont_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.AutoSize = false;
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(10, 25);
            // 
            // tsbFontColor
            // 
            this.tsbFontColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbFontColor.Image = global::ChatTool.Properties.Resources.FontColor;
            this.tsbFontColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbFontColor.Name = "tsbFontColor";
            this.tsbFontColor.Size = new System.Drawing.Size(24, 24);
            this.tsbFontColor.Text = "颜色";
            this.tsbFontColor.Click += new System.EventHandler(this.tsbSetFont_Click);
            // 
            // gbMembers
            // 
            this.gbMembers.Controls.Add(this.tvMembers);
            this.gbMembers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbMembers.Location = new System.Drawing.Point(0, 0);
            this.gbMembers.Name = "gbMembers";
            this.gbMembers.Size = new System.Drawing.Size(169, 370);
            this.gbMembers.TabIndex = 0;
            this.gbMembers.TabStop = false;
            this.gbMembers.Text = "群内成员";
            // 
            // tvMembers
            // 
            this.tvMembers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tvMembers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvMembers.ImageIndex = 0;
            this.tvMembers.ImageList = this.imageList;
            this.tvMembers.Location = new System.Drawing.Point(3, 17);
            this.tvMembers.Name = "tvMembers";
            this.tvMembers.SelectedImageIndex = 0;
            this.tvMembers.ShowLines = false;
            this.tvMembers.Size = new System.Drawing.Size(163, 350);
            this.tvMembers.TabIndex = 0;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "user.ico");
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // FormChatToGroup
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(567, 465);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormChatToGroup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "群聊天室";
            this.Load += new System.EventHandler(this.FormChatToGroup_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.FormChatToGroup_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.FormChatToGroup_DragEnter);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormChatToGroup_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbHisPhoto)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tsUp.ResumeLayout(false);
            this.tsUp.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tsMid.ResumeLayout(false);
            this.tsMid.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tsDown.ResumeLayout(false);
            this.tsDown.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tsFont.ResumeLayout(false);
            this.tsFont.PerformLayout();
            this.gbMembers.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox pbHisPhoto;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonMax;
        private System.Windows.Forms.Button buttonMin;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelMood;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.ToolStrip tsUp;
        private System.Windows.Forms.ToolStripButton tsbFileTransfer;
        private System.Windows.Forms.ToolStripButton tsbDesktopShare;
        private System.Windows.Forms.ToolStripButton tsbVoiceChat;
        private System.Windows.Forms.ToolStripButton tsbVideoChat;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ToolStrip tsMid;
        private System.Windows.Forms.ToolStripButton tsbSetFont;
        private System.Windows.Forms.ToolStripButton tsbFaceManager;
        private System.Windows.Forms.ToolStripButton tsbSendPicture;
        private System.Windows.Forms.ToolStripButton tsbMsgRecord;
        private System.Windows.Forms.ToolStripSplitButton ttssbSendCutPicture;
        private System.Windows.Forms.ToolStripMenuItem 截图时隐藏聊天窗口ToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.ToolStrip tsDown;
        private System.Windows.Forms.ToolStripSplitButton tsbSend;
        private System.Windows.Forms.ToolStripMenuItem 按Enter发送消息ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 按CtrlEnter发送消息ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbClose;
        private System.Windows.Forms.RichTextBox rtbWrite;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RichTextBox rtbRecord;
        private System.Windows.Forms.ToolStrip tsFont;
        private System.Windows.Forms.ToolStripComboBox tscbFontFamily;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripComboBox tscbFontSize;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton tsbAddRough;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tsbIncline;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton tsbUnderline;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton tsbStrikeout;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton tsbFontColor;
        private System.Windows.Forms.GroupBox gbMembers;
        private System.Windows.Forms.TreeView tvMembers;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.ImageList imageList;
    }
}