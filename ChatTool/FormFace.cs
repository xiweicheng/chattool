﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ChatTool
{
    public partial class FormFace : Form
    {
        private FormChatToOne formChatToOne;

        public FormFace()
        {
            InitializeComponent();
        }

        public FormFace(FormChatToOne formChatToOne)
        {
            InitializeComponent();
            this.formChatToOne = formChatToOne;
        }
        private FormChatToGroup formChatToGroup;
        public FormFace(FormChatToGroup formChatToGroup)
        {
            InitializeComponent();
            this.formChatToGroup = formChatToGroup;
        }

        private void FormFace_Load(object sender, EventArgs e)
        {
            Bitmap[] faces = FileOperate.GetFaces();
            if (faces == null)
            {
                MessageBox.Show("加载表情时异常!");
                return;
            }
            foreach (Bitmap face in faces)
            {
                PictureBox pb = new PictureBox();
                pb.Width = 24;
                pb.Height = 24;
                pb.SizeMode = PictureBoxSizeMode.Zoom;
                pb.BorderStyle = BorderStyle.FixedSingle;
                pb.Image = face;
                pb.Click += new EventHandler(pb_Click);
                pb.MouseEnter += new EventHandler(pb_MouseEnter);
                pb.MouseLeave += new EventHandler(pb_MouseLeave);
                this.flpFace.Controls.Add(pb);
            }
        }

        private void pb_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = sender as PictureBox;
            pb.Width -= 10;
            pb.Height -= 10;
        }

        private void pb_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = sender as PictureBox;
            pb.Width += 10;
            pb.Height += 10;
        }

        private void pb_Click(object sender, EventArgs e)
        {
            PictureBox pb = sender as PictureBox;
            Bitmap sendImage = new Bitmap(pb.Image);
            if (formChatToOne != null)
                formChatToOne.AddFaceToRbWrite(sendImage);
            else if (formChatToGroup != null)
                formChatToGroup.HandleBitmap(sendImage);
        }

        private void FormFace_Deactivate(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}
