﻿namespace ChatTool
{
    partial class FormSetCopyRegion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // FormSetCopyRegion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(141, 119);
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormSetCopyRegion";
            this.Opacity = 0.05D;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "FormSetCopyRegion";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormSetCopyRegion_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormSetCopyRegion_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormSetCopyRegion_KeyUp);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormSetCopyRegion_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FormSetCopyRegion_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FormSetCopyRegion_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion

    }
}