﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;

namespace ChatTool
{
    public partial class FormVideoControl : Form
    {
        private FormChatToOne formChatToOne;

        public FormChatToOne FormChatToOne
        {
            get { return formChatToOne; }
        }
        private Cvideo cVideo;
        private UserClient userClient;
        private VideoClient videoClient;
        public FormVideoControl(FormChatToOne formChatToOne)
        {
            InitializeComponent();
            this.formChatToOne = formChatToOne;
            cVideo = new Cvideo();
        }

        private void FormVideoControl_FormClosing(object sender, FormClosingEventArgs e)
        {
            formChatToOne.SetButtonEnabled("tsbVideoChat", true);
            timer.Stop();
            if (videoClient != null)
            {
                videoClient.CloseVideoClient();
            }
            if (cVideo != null)
            {
                cVideo.CloseWebcam();
            }
        }
        /// 打开摄像头预览视频
        private void buttonOpenVideo_Click(object sender, EventArgs e)
        {
            if (buttonOpenVideo.Text == "开启视频")
            {
                if (cVideo.StartWebCam(pictureBoxVideo.Handle, pictureBoxVideo.Width, pictureBoxVideo.Height) == false)
                {
                    cVideo.CloseWebcam();
                    MessageBox.Show("摄像头连接失败!");
                    return;
                }
                this.buttonOpenVideo.Text = "关闭视频";
                this.buttonSaveVideo.Enabled = true;
                this.buttonTakePhoto.Enabled = true;
                this.buttonVideoTalk.Enabled = true;
            }
            else
            {
                if (this.buttonVideoTalk.Text == "停止视聊")
                    videoClient.CloseVideoClient();
                if (buttonSaveVideo.Text == "停止录制")
                    cVideo.StopKinescope();
                this.Close();
            }
        }
        //拍照自恋
        private void buttonTakePhoto_Click(object sender, EventArgs e)
        {
            string photoName = DateTime.Now.ToString().Replace(':', '-').Replace(' ', '@').Replace('/', '-') + ".jpg";
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + photoName;
            cVideo.GrabImage(path);
            MessageBox.Show("拍照成功,图片已经保存在桌面上."); 
        }
        //录制视频
        private void buttonSaveVideo_Click(object sender, EventArgs e)
        {
            if (buttonSaveVideo.Text == "录制视频")
            {
                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    buttonSaveVideo.Text = "停止录制";
                    string path = folderBrowserDialog.SelectedPath + "\\";
                    string videoName = DateTime.Now.ToString().Replace(':', '-').Replace(' ', '@').Replace('/', '-') + ".avi";
                    Thread.Sleep(100);
                    cVideo.StarKinescope(path + videoName);
                }
            }
            else
            {
                buttonSaveVideo.Text = "录制视频";
                cVideo.StopKinescope();
            }
        }
        //视频聊天
        private void buttonVideoTalk_Click(object sender, EventArgs e)
        {
            if (buttonVideoTalk.Text == "视频聊天")
            {
                videoClient = new VideoClient(this, cVideo);
                videoClient.StartConnectServer();
            }
            else
            {
                buttonVideoTalk.Text = "视频聊天";
                videoClient.CloseVideoClient();
            }
        }
        private delegate void SetButtonTextDelegate(string buttonName, string text);
        public void SetButtonText(string buttonName, string text)
        {
            SetButtonTextDelegate d = new SetButtonTextDelegate(SetButtonText);
            if (buttonName == "buttonVideoTalk")
            {
                if (buttonVideoTalk.InvokeRequired)
                    buttonVideoTalk.Invoke(d, buttonName, text);
                else
                    buttonVideoTalk.Text = text;
            }
        }
        private delegate void StartSendImageDelegate(UserClient userClient);
        public void StartSendImage(UserClient userClient)
        {
            this.Invoke(new StartSendImageDelegate(SendImage), userClient);
        }
        private void SendImage(UserClient userClient)
        {
            this.userClient = userClient;
            timer.Start();
        }
        private void timer_Tick(object sender, EventArgs e)
        {
            Image image = null;
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    cVideo.CaptureWindow();
                    if (Clipboard.ContainsImage())
                    {
                        image = Clipboard.GetImage();
                        image.Save(ms, ImageFormat.Jpeg);
                        byte[] bs = ms.GetBuffer();
                        NetManager.SendBytesMsgToServer(userClient, bs);
                        image.Dispose();
                        image = null;    
                    }
                }
            }
            catch (Exception)
            {
                timer.Stop();
                this.buttonVideoTalk.Text = "视频聊天";
            }
        }
    }
}
