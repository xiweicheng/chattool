﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Media;

namespace ChatTool
{
    public class Tool
    {
        public static bool isFormManagerTopmost = false;
        public static SortedList<string, string> peiZhiData = new SortedList<string, string>();
        public static Font Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        /// <summary>
        /// 将一般字符串转换为Rtf格式字符串
        /// </summary>
        public static string GetRtfString(string text, Font font, Color color)
        {
            RichTextBox rtb = new RichTextBox();
            rtb.Font = font;
            rtb.ForeColor = color;
            rtb.Text = text;
            return rtb.Rtf;
        }
        /// <summary>
        /// 将Rtf格式字符串去头
        /// </summary>
        public static string FilterRtfHead(string rtf)
        {
            int index = rtf.IndexOf(@"\fs18");
            //int index = rtf.IndexOf("rtf1");
            return rtf.Substring(index + 4);
            //return rtf.Substring(0);
        }
        /// <summary>
        /// 将Rtf格式字符串去尾
        /// </summary>
        public static string FilterRtfTail(string rtf)
        {
            int index = rtf.LastIndexOf(@"\par");
            return rtf.Substring(0, index);
        }
        /// <summary>
        /// 将字节位数的长度转换为KB或者MB或者GB
        /// </summary>
        public static string GetFileSizeFromByteCount(long byteCount)
        {
            double fileSizeMB = 1.0 * byteCount / 1024 / 1024;
            double fileSizeGB = fileSizeMB / 1024;
            string fileSize = "";
            if (fileSizeGB >= 1)
                fileSize = string.Format("{0:0.##} GB", fileSizeGB);
            else
            {
                if (fileSizeMB >= 1)
                    fileSize = string.Format("{0:0.##} MB", fileSizeMB);
                else
                    fileSize = ((int)(fileSizeMB * 1024)).ToString() + " KB";
            }
            return fileSize;
        }
        /// <summary>
        /// 播放声音
        /// </summary>
        /// <param name="ums"></param>
        public static void Play(System.IO.UnmanagedMemoryStream ums)
        {
            SoundPlayer sp = new SoundPlayer(ums);
            sp.Play();
        }
    }
}
