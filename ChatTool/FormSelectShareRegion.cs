﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ChatTool
{
    public partial class FormSelectShareRegion : Form
    {
        private Point mouseDownLocation;
        private FormDrawBoard drawBorad;//画板
        private Rectangle rectDraw;//截取区域
        private FormChatToOne formChatToOne;
        private bool isCtrlDown = false;//ctrl键是否按下
        //截图区域的左上角
        public Point MouseDownLocation
        {
            get { return mouseDownLocation; }
            set { this.mouseDownLocation = value; }
        }
        private UserClient desktopShareUserClient;

        public FormSelectShareRegion()
        {
            InitializeComponent();
        }
      
        public FormSelectShareRegion(FormChatToOne formChatToOne, UserClient desktopShareUserClient)
        {
            InitializeComponent();
            this.formChatToOne = formChatToOne;
            this.desktopShareUserClient = desktopShareUserClient;
        }
        private void FormSelectShareRegion_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.mouseDownLocation = e.Location;
            }
        }

        private void FormSelectShareRegion_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (e.X > mouseDownLocation.X && e.Y > mouseDownLocation.Y)
                    rectDraw = new Rectangle(mouseDownLocation, new Size(e.X - mouseDownLocation.X, e.Y - mouseDownLocation.Y));
                else if (e.X > mouseDownLocation.X && e.Y < mouseDownLocation.Y)
                    rectDraw = new Rectangle(mouseDownLocation.X, e.Y, e.X - mouseDownLocation.X, mouseDownLocation.Y - e.Y);
                else if (e.X < mouseDownLocation.X && e.Y < mouseDownLocation.Y)
                    rectDraw = new Rectangle(e.X, e.Y, mouseDownLocation.X - e.X, mouseDownLocation.Y - e.Y);
                else
                    rectDraw = new Rectangle(e.X, mouseDownLocation.Y, mouseDownLocation.X - e.X, e.Y - mouseDownLocation.Y);
                drawBorad.Draw(true, rectDraw, e);
            }
            else
                drawBorad.Draw(false, rectDraw, e);
        }

        private void FormSelectShareRegion_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
                this.isCtrlDown = true;
        }

        private void FormSelectShareRegion_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (this.isCtrlDown == true)
                    return;
                this.formChatToOne.ShareRegion = this.rectDraw;
                NetManager.SendStringMsgToServer(desktopShareUserClient, "DesktopShare");
                drawBorad.Close();
                this.Close();
            }
        }

        private void FormSelectShareRegion_Load(object sender, EventArgs e)
        {
            drawBorad = new FormDrawBoard();//画板
            drawBorad.Show();
        }

        private void FormSelectShareRegion_KeyUp(object sender, KeyEventArgs e)
        {
            this.isCtrlDown = false;
        }
    }
}
