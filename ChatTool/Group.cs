﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChatTool
{
    [Serializable]
    public class Group
    {
        private string name;
        /// <summary>
        /// 组名
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string creator;
        /// <summary>
        /// 创建者
        /// </summary>
        public string Creator
        {
            get { return creator; }
            set { creator = value; }
        }
        private string creatorName;
        /// <summary>
        /// 创建者姓名
        /// </summary>
        public string CreatorName
        {
            get { return creatorName; }
            set { creatorName = value; }
        }
        private string groupTheme;
        /// <summary>
        /// 群内主题
        /// </summary>
        public string GroupTheme
        {
            get { return groupTheme; }
            set { groupTheme = value; }
        }
        private GroupState state;
        /// <summary>
        /// 应用改组的状态
        /// </summary>
        public GroupState State
        {
            get { return state; }
            set { state = value; }
        }
        private string requestor;
        /// <summary>
        /// 要求发出者
        /// </summary>
        public string Requestor
        {
            get { return requestor; }
            set { requestor = value; }
        }
        private bool isSelected;
        /// <summary>
        /// 指示该群组是否已经选定
        /// </summary>
        public bool IsSelected
        {
            get { return isSelected; }
            set { isSelected = value; }
        }
        private User user;
        /// <summary>
        /// 成员信息
        /// </summary>
        public User User
        {
            get { return user; }
            set { user = value; }
        }
        public override string ToString()
        {
            return "群组名:{" + name + "} 创建者[" + creatorName + "]@ip[" + creator + "]";
        }
    }

    [Serializable]
    public enum GroupState
    {
        /// <summary>
        /// 创建
        /// </summary>
        Create,
        /// <summary>
        /// 删除
        /// </summary>
        Delete,
        /// <summary>
        /// 加入
        /// </summary>
        Join,
        /// <summary>
        /// 退出
        /// </summary>
        Leave,
        /// <summary>
        /// 要求回应创建组
        /// </summary>
        CallbackGroup,
        /// <summary>
        /// 回应信息
        /// </summary>
        ReturnBack,
        /// <summary>
        /// 呼叫群组成员
        /// </summary>
        CallUser,
        /// <summary>
        /// 回应成员呼叫
        /// </summary>
        ReturnUser
    }
}
