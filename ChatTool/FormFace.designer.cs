﻿namespace ChatTool
{
    partial class FormFace
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpFace = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // flpFace
            // 
            this.flpFace.AutoScroll = true;
            this.flpFace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flpFace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpFace.Location = new System.Drawing.Point(0, 0);
            this.flpFace.Name = "flpFace";
            this.flpFace.Size = new System.Drawing.Size(319, 175);
            this.flpFace.TabIndex = 0;
            // 
            // FormFace
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 175);
            this.Controls.Add(this.flpFace);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormFace";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormFace";
            this.Deactivate += new System.EventHandler(this.FormFace_Deactivate);
            this.Load += new System.EventHandler(this.FormFace_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpFace;
    }
}