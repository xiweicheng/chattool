﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChatTool
{
    [Serializable]
    public class ChatRecord
    {
        private string startDateTime;
        /// <summary>
        /// 聊天的开始时间
        /// </summary>
        public string StartDateTime
        {
            get { return startDateTime; }
            set { startDateTime = value; }
        }
        private string endDateTime;
        /// <summary>
        /// 聊天的结束时间
        /// </summary>
        public string EndDateTime
        {
            get { return endDateTime; }
            set { endDateTime = value; }
        }
        private string chatContent;
        /// <summary>
        /// 聊天的内容
        /// </summary>
        public string ChatContent
        {
            get { return chatContent; }
            set { chatContent = value; }
        }
        public override string ToString()
        {
            return this.startDateTime + "至" + this.endDateTime;
        }
    }
}
