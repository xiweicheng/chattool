﻿namespace ChatTool
{
    partial class FormVideoControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormVideoControl));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBoxVideo = new System.Windows.Forms.PictureBox();
            this.groupBoxOpenVideo = new System.Windows.Forms.GroupBox();
            this.buttonVideoTalk = new System.Windows.Forms.Button();
            this.buttonTakePhoto = new System.Windows.Forms.Button();
            this.buttonSaveVideo = new System.Windows.Forms.Button();
            this.buttonOpenVideo = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVideo)).BeginInit();
            this.groupBoxOpenVideo.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.pictureBoxVideo);
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(8, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(307, 228);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "视频预览";
            // 
            // pictureBoxVideo
            // 
            this.pictureBoxVideo.BackColor = System.Drawing.Color.White;
            this.pictureBoxVideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxVideo.Location = new System.Drawing.Point(3, 17);
            this.pictureBoxVideo.Name = "pictureBoxVideo";
            this.pictureBoxVideo.Size = new System.Drawing.Size(301, 208);
            this.pictureBoxVideo.TabIndex = 0;
            this.pictureBoxVideo.TabStop = false;
            // 
            // groupBoxOpenVideo
            // 
            this.groupBoxOpenVideo.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxOpenVideo.Controls.Add(this.buttonVideoTalk);
            this.groupBoxOpenVideo.Controls.Add(this.buttonTakePhoto);
            this.groupBoxOpenVideo.Controls.Add(this.buttonSaveVideo);
            this.groupBoxOpenVideo.Controls.Add(this.buttonOpenVideo);
            this.groupBoxOpenVideo.ForeColor = System.Drawing.Color.Black;
            this.groupBoxOpenVideo.Location = new System.Drawing.Point(321, 12);
            this.groupBoxOpenVideo.Name = "groupBoxOpenVideo";
            this.groupBoxOpenVideo.Size = new System.Drawing.Size(88, 225);
            this.groupBoxOpenVideo.TabIndex = 1;
            this.groupBoxOpenVideo.TabStop = false;
            this.groupBoxOpenVideo.Text = "视频控制";
            // 
            // buttonVideoTalk
            // 
            this.buttonVideoTalk.Enabled = false;
            this.buttonVideoTalk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonVideoTalk.ForeColor = System.Drawing.Color.Black;
            this.buttonVideoTalk.Location = new System.Drawing.Point(7, 73);
            this.buttonVideoTalk.Name = "buttonVideoTalk";
            this.buttonVideoTalk.Size = new System.Drawing.Size(75, 29);
            this.buttonVideoTalk.TabIndex = 3;
            this.buttonVideoTalk.Text = "视频聊天";
            this.buttonVideoTalk.UseVisualStyleBackColor = true;
            this.buttonVideoTalk.Click += new System.EventHandler(this.buttonVideoTalk_Click);
            // 
            // buttonTakePhoto
            // 
            this.buttonTakePhoto.Enabled = false;
            this.buttonTakePhoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTakePhoto.ForeColor = System.Drawing.Color.Black;
            this.buttonTakePhoto.Location = new System.Drawing.Point(7, 122);
            this.buttonTakePhoto.Name = "buttonTakePhoto";
            this.buttonTakePhoto.Size = new System.Drawing.Size(75, 29);
            this.buttonTakePhoto.TabIndex = 2;
            this.buttonTakePhoto.Text = "拍照自恋";
            this.buttonTakePhoto.UseVisualStyleBackColor = true;
            this.buttonTakePhoto.Click += new System.EventHandler(this.buttonTakePhoto_Click);
            // 
            // buttonSaveVideo
            // 
            this.buttonSaveVideo.Enabled = false;
            this.buttonSaveVideo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSaveVideo.ForeColor = System.Drawing.Color.Black;
            this.buttonSaveVideo.Location = new System.Drawing.Point(7, 172);
            this.buttonSaveVideo.Name = "buttonSaveVideo";
            this.buttonSaveVideo.Size = new System.Drawing.Size(75, 29);
            this.buttonSaveVideo.TabIndex = 1;
            this.buttonSaveVideo.Text = "录制视频";
            this.buttonSaveVideo.UseVisualStyleBackColor = true;
            this.buttonSaveVideo.Click += new System.EventHandler(this.buttonSaveVideo_Click);
            // 
            // buttonOpenVideo
            // 
            this.buttonOpenVideo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOpenVideo.ForeColor = System.Drawing.Color.Black;
            this.buttonOpenVideo.Location = new System.Drawing.Point(7, 27);
            this.buttonOpenVideo.Name = "buttonOpenVideo";
            this.buttonOpenVideo.Size = new System.Drawing.Size(75, 29);
            this.buttonOpenVideo.TabIndex = 0;
            this.buttonOpenVideo.Text = "开启视频";
            this.buttonOpenVideo.UseVisualStyleBackColor = true;
            this.buttonOpenVideo.Click += new System.EventHandler(this.buttonOpenVideo_Click);
            // 
            // timer
            // 
            this.timer.Interval = 66;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // FormVideoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(419, 253);
            this.Controls.Add(this.groupBoxOpenVideo);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormVideoControl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "摄像头控制";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormVideoControl_FormClosing);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVideo)).EndInit();
            this.groupBoxOpenVideo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBoxVideo;
        private System.Windows.Forms.GroupBox groupBoxOpenVideo;
        private System.Windows.Forms.Button buttonOpenVideo;
        private System.Windows.Forms.Button buttonVideoTalk;
        private System.Windows.Forms.Button buttonTakePhoto;
        private System.Windows.Forms.Button buttonSaveVideo;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
    }
}