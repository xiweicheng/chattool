﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ChatTool
{
    public partial class FormSkinSet : Form
    {
        private FormManager formManager;
        public FormSkinSet(FormManager formManager)
        {
            InitializeComponent();
            this.formManager = formManager;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            PictureBox pb = sender as PictureBox;
            if (pb.Image != null)
            {
                this.formManager.SetBackgroudSkinImage(new Bitmap(pb.Image));
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            this.formManager.SetFormManagerOpacity(trackBar1.Value);
        }

        private void FormSkinSet_Deactivate(object sender, EventArgs e)
        {
            this.Visible = false;
            formManager.Select();
            formManager.Activate();
        }
    }
}
