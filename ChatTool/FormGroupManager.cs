﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ChatTool
{
    public partial class FormGroupManager : Form
    {
        private List<Group> listGroup;
        private FormManager formManager;
        private NetManager netManager;
        public FormGroupManager(FormManager formManager)
        {
            InitializeComponent();
            this.formManager = formManager;
            this.netManager = this.formManager.NetManager;
        }

        private void FormGroupManager_Load(object sender, EventArgs e)
        {
            this.netManager.GroupInfoReceived += new NetManager.GroupInfoReceivedDelegate(netManager_GroupInfoReceived);

            LoadGroup();
        }

        private void LoadGroup()
        {
            this.lbOwnGroup.Items.Clear();
            this.listGroup = FileOperate.GroupFromFile();
            if (this.listGroup != null)
            {
                this.lbOwnGroup.Items.Clear();
                foreach (Group item in this.listGroup)
                {
                    if (item.Creator == NetManager.GetHostIP().ToString())
                    {
                        this.lbOwnGroup.Items.Add(item);
                    }
                }
            }
            else
            {
                this.listGroup = new List<Group>();
            }
        }

        private void buttonAddNewGroup_Click(object sender, EventArgs e)
        {
            string newGroupName = this.tbNewGroupName.Text.Trim();
            if (newGroupName == "")
                return;
            for (int i = 0; i < lbOwnGroup.Items.Count; i++)
            {
                Group group = lbOwnGroup.Items[i] as Group;
                if (group.Name == newGroupName)
                {
                    MessageBox.Show("该群组名已经存在,请重新命名.");
                    return;
                }
            }
            Group newGroup = new Group();
            newGroup.Name = newGroupName;
            newGroup.Creator = NetManager.GetHostIP().ToString();
            newGroup.CreatorName = this.formManager.UserOwn.Name;
            newGroup.GroupTheme = this.tbSaySome.Text.Trim();
            this.listGroup.Add(newGroup);
            bool flag = FileOperate.WriteGroupToFile(this.listGroup);
            if (flag == false)
            {
                this.listGroup.Remove(newGroup);
                MessageBox.Show("创建新群组失败.");
                return;
            }
            this.lbOwnGroup.Items.Add(newGroup);
            this.formManager.AddOrDeleteGroup("Add", newGroup);
        }

        private void buttonDeleteGroup_Click(object sender, EventArgs e)
        {
            if (lbOwnGroup.SelectedIndex != -1)
            {
                Group group = lbOwnGroup.SelectedItem as Group;
                this.listGroup.Remove(group);
                bool flag = FileOperate.WriteGroupToFile(this.listGroup);
                if (flag == false)
                {
                    MessageBox.Show("删除群组失败.");
                    this.listGroup.Add(group);
                    return;
                }
                this.lbOwnGroup.Items.RemoveAt(lbOwnGroup.SelectedIndex);
                this.formManager.AddOrDeleteGroup("Delete", group);
            }
        }

        private void netManager_GroupInfoReceived(Group group)
        {
            if (group.State == GroupState.CallbackGroup)
            {
                List<Group> listGroup = FileOperate.GroupFromFile();
                if (listGroup != null)
                {
                    for (int i = 0; i < listGroup.Count; i++)
                    {
                        if (listGroup[i].Creator == NetManager.GetHostIP().ToString())
                        {
                            listGroup[i].Requestor = group.Requestor;
                            listGroup[i].State = GroupState.ReturnBack;
                            netManager.StartBroadcastThread(listGroup[i]);
                        }
                    }
                }
            }
            else if (group.State == GroupState.ReturnBack)
            {
                if (group.Requestor == NetManager.GetHostIP().ToString())
                    this.Invoke(new AddGroupDelegate(AddGroup), group);
            }
        }
        private delegate void AddGroupDelegate(Group group);
        private void AddGroup(Group group)
        {
            if (ListGroupContains(group) == false)
            {
                this.lbNetGroup.Items.Add(group);
            }

        }
        private bool ListGroupContains(Group group)
        {
            foreach ( Object item in lbNetGroup.Items)
            {
                Group g = item as Group;
                if (g.Name == group.Name && g.Creator == group.Creator)
                    return true;
            }
            return false;
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl.SelectedIndex == 1)
            {
                CallGroup();
            }
            else if (tabControl.SelectedIndex == 0)
            {
                LoadGroup();
            }
        }

        private void CallGroup()
        {
            Group group = new Group();
            group.Requestor = NetManager.GetHostIP().ToString();
            group.State = GroupState.CallbackGroup;
            this.lbNetGroup.Items.Clear();
            netManager.StartBroadcastThread(group);

            List<Group> listGroups = FileOperate.GroupFromFile();
            this.lbHaveJoinGroup.Items.Clear();
            if (listGroups != null)
            {
                foreach (Group item in listGroups)
                {
                    this.lbHaveJoinGroup.Items.Add(item);
                }
            }
        }
        //加入网络群组
        private void buttonJoin_Click(object sender, EventArgs e)
        {
            if (lbNetGroup.SelectedIndex == -1)
                return;
            Group group = lbNetGroup.SelectedItem as Group;
            if (IsHaveJoin(group) == false)
            {
                this.lbHaveJoinGroup.Items.Add(group);
                this.formManager.AddOrDeleteGroup("Add", group);
                this.listGroup.Add(group);
                FileOperate.WriteGroupToFile(listGroup);
            }
            else
            {
                MessageBox.Show("该选定群组您已经加入过了.");
            }
        }
        //判断是否已经加入某个网络群组
        private bool IsHaveJoin(Group group)
        {
            foreach (Object item in lbHaveJoinGroup.Items)
            {
                Group group1 = item as Group;
                if (group1.Name == group.Name && group1.Creator == group.Creator)
                    return true;
            }
            return false;
        }

        private void buttonOut_Click(object sender, EventArgs e)
        {
            if (lbHaveJoinGroup.SelectedIndex == -1)
                return;
            Group group = lbHaveJoinGroup.SelectedItem as Group;
            this.lbHaveJoinGroup.Items.Remove(group);
            this.formManager.AddOrDeleteGroup("Delete", group);
            DeleteGroupFromList(group);
            FileOperate.WriteGroupToFile(listGroup);
            CallGroup();
        }

        private void DeleteGroupFromList(Group group)
        {
            Group se = null;
            foreach (Group item in listGroup)
            {
                if (item.Name == group.Name && item.Creator == group.Creator)
                {
                    se = item;
                    break;
                }
            }
            if (se != null)
                this.listGroup.Remove(se);
        }

        private void FormGroupManager_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.netManager.GroupInfoReceived -= new NetManager.GroupInfoReceivedDelegate(netManager_GroupInfoReceived);

        }
    }
}
