﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace ChatTool
{
    public partial class FormDrawBoard : Form
    {
        private bool flag = false;//是否画区域的标志
        private MouseEventArgs ee;
        private Rectangle rectDraw; 
        public FormDrawBoard()
        {
            InitializeComponent();
        }

        public void Draw(bool flag, Rectangle rectDraw, MouseEventArgs e)
        {
            this.flag = flag;
            this.rectDraw = rectDraw;
            this.ee = e;
            this.Refresh();
        }

        private void FormDrawBoard_Paint(object sender, PaintEventArgs e)
        {
            if (this.ee == null)
                return;
            Graphics g = e.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.Clear(this.BackColor);
            g.DrawLine(Pens.Red, 0, this.ee.Y, this.Width, this.ee.Y);
            g.DrawLine(Pens.Red, this.ee.X, 0, this.ee.X, this.Height);
            if (flag)
            {
                Font font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size * 1.5F, FontStyle.Bold);
                SizeF size = g.MeasureString(rectDraw.Width.ToString(), font);
                int dh = 20;
                int wid = (int)(rectDraw.Width - size.Width) / 2;
                g.DrawLine(Pens.Red, rectDraw.X, rectDraw.Y, rectDraw.X, rectDraw.Y - 2 * dh);
                g.DrawLine(Pens.Red, rectDraw.X + rectDraw.Width, rectDraw.Y, rectDraw.X + rectDraw.Width, rectDraw.Y - 2 * dh);

                Pen myPen = new Pen(Color.Red);
                AdjustableArrowCap arrow = new AdjustableArrowCap(4, 4, true);
                myPen.CustomEndCap = arrow;
                g.DrawLine(myPen, rectDraw.X, rectDraw.Y - dh, rectDraw.X + wid, rectDraw.Y - dh);
                g.DrawLine(myPen, rectDraw.X + rectDraw.Width, rectDraw.Y - dh, rectDraw.X + rectDraw.Width - wid, rectDraw.Y - dh);
                g.DrawString(rectDraw.Width.ToString(), font, Brushes.Red, new PointF(rectDraw.X + wid, rectDraw.Y - dh - size.Height / 2));

                size = g.MeasureString(rectDraw.Height.ToString(), font);
                int hei = (int)(rectDraw.Height - size.Width) / 2;
                g.DrawLine(Pens.Red, rectDraw.X - 2 * dh, rectDraw.Y, rectDraw.X, rectDraw.Y);
                g.DrawLine(Pens.Red, rectDraw.X, rectDraw.Y + rectDraw.Height, rectDraw.X - 2 * dh, rectDraw.Y + rectDraw.Height);

                g.DrawLine(myPen, rectDraw.X - dh, rectDraw.Y, rectDraw.X - dh, rectDraw.Y + hei);
                g.DrawLine(myPen, rectDraw.X - dh, rectDraw.Y + rectDraw.Height, rectDraw.X - dh, rectDraw.Y + rectDraw.Height - hei);

                arrow.Dispose();
                myPen.Dispose();

                StringFormat format = new StringFormat(StringFormatFlags.DirectionVertical);
                g.DrawString(rectDraw.Height.ToString(), font, Brushes.Red, new PointF(rectDraw.X - dh - size.Height / 2, rectDraw.Y + hei), format);
                format.Dispose();

                g.DrawString(rectDraw.X + "," + rectDraw.Y, font, Brushes.Red, new PointF(rectDraw.X + dh / 2, rectDraw.Y + dh / 2));
                g.DrawString((rectDraw.X+rectDraw.Width) + "," + (rectDraw.Y+rectDraw.Height), font, Brushes.Red, new PointF(rectDraw.X +rectDraw.Width + dh / 2, rectDraw.Y +rectDraw.Height + dh / 2));

                font.Dispose();

                Pen pen = new Pen(Color.Red, 2.0F);
                g.DrawRectangle(pen, rectDraw);
                pen.Dispose();
                pen = null;
            }
        }
    }
}
