﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.IO;

namespace ChatTool
{
    public class UserClient
    {
        public TcpClient TcpClient { get; private set; }
        public BinaryReader Br { get; private set; }
        public BinaryWriter Bw { get; private set; }
        public User User { get; set; }
        public UserClient(TcpClient tcpClient)
        {
            this.TcpClient = tcpClient;
            NetworkStream ns = tcpClient.GetStream();
            Br = new BinaryReader(ns);
            Bw = new BinaryWriter(ns);
        }
        /// <summary>
        /// 关闭连接
        /// </summary>
        public void Close()
        {
            Br.Close();
            Bw.Close();
            TcpClient.Client.Close();
            TcpClient.Close();
        }
    }
}
